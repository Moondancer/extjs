<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login</title>
    <link rel="stylesheet"
        href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
</head>
<body>

	<div class="container">

		<h1>Login</h1>

		<c:if test="${not empty param.login_error}">
            <div class="alert alert-danger">Login failed</div>
        </c:if>

		<form role="form"
			action="<c:url value='/j_spring_security_check'/>"
			method="post">

				<div class="form-group">
					<label for="j_username">Username</label>
					<input type="text" name="j_username" />
				</div>

				<div class="form-group">
					<label for="j_password">Password</label>
					<input type="password" name="j_password" />
				</div>


			<button type="submit" class="btn btn-default">Submit</button>
		</form>

	</div>

</body>
</html>
