﻿Ext.define('MovieWorld.model.Role',{
    extend: 'Ext.data.Model',
    idPropery: 'roleId',
    fields: [
        {name: 'roleId',  type: 'int'},
        {name: 'actorId', type: 'int'},
        {name: 'movieId', type: 'int'},
        {name: 'characterName',  type: 'string'}
    ]
});