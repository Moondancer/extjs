/**
 * Created by Training on 2015.02.11..
 */

var task;

function counter(number) {
    Ext.get('counter').update(number);
}

function removingSplash() {
    // Removing splash screen
    E5.splashScreen.fadeOut({
        duration: 10000,
        remove: true
    });
    E5.splashScreen.next().fadeOut({
        duration: 10000,
        remove: true
    });
}

function startSplash(){
    // Starting splash screen
    E5.splashScreen = Ext.getBody().mask("Application E5 is loading...");
    E5.splashScreen.fadeIn(0);
}

function createAndStartTask() {
    var number = 0;
    var runner = new Ext.util.TaskRunner();
    task = runner.newTask({
        run: function () {
            number++;
            counter(number);
        },
        interval: 1000
    });
    task.start();
}

function createStartStopButton() {
    Ext.create('Ext.Button', {
        text: 'Stop',
        renderTo: Ext.get('line'),
        listeners: {
            click: function() {
                var text = this.getText();
                if(text == "Stop") {
                    text = "Start";
                    task.stop();
                } else {
                    text = "Stop";
                    task.start();
                }
                this.setText(text);
            }
        }
    });
}

function createWarnOnCloseEvent() {
    Ext.EventManager.on(
        window,
        'beforeunload',
        function(e) {
            e.stopEvent();
            if(e.browserEvent){
                e.browserEvent.returnValue = "WARNING!";
            }
        }
    );
}

Ext.application(
    {
        name: 'E5',
        extend: 'Ext.app.Application',
        splashScreen: {},
        launch: function () {
            removingSplash();
            createAndStartTask();
            createStartStopButton();
        },
        init: function() {
            createWarnOnCloseEvent();
            startSplash();
        }
    }
);