package com.training360.backend.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.training360.backend.domain.Book;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface BookRepository extends PagingAndSortingRepository<Book, Long> {

    @Query("select book from Book book where book.author like :query% or book.title like :query%")
    Page<Book> findBooksByQuery(@Param("query") String query, Pageable pageable);
    
    @Query(value = "select distinct book from Book book join fetch book.reservations where book.author like :query% or book.title like :query%",
        countQuery = "select count(distinct book) from Book book join book.reservations where book.author like :query% or book.title like :query%")
            
    Page<Book> findBooksWithReservations(@Param("query") String query, Pageable pageable);

    @Query("select distinct book from Book book left join fetch book.reservations where book.id = :id")
    Book findBookWithReservations(@Param("id") Long id);

    @Query(value = "select distinct book from Book book left join fetch book.reservations reservation where (book.borrowedBy = :user or reservation.user = :user) and (book.author like :query% or book.title like :query%)",
        countQuery = "select count(distinct book) from Book book left join book.reservations reservation where (book.borrowedBy = :user or reservation.user = :user) and (book.author like :query% or book.title like :query%)")
    Page<Book> findMyBooks(@Param("user") String user, @Param("query") String query,Pageable pageable);

    Book findBookByAuthorAndTitle(String author, String title);
}
