Ext.define(
        'MovieWorld.view.MainMenu',
        {
            extend: 'Ext.menu.Menu',
            alias: 'widget.mainmenu',
            floating: true, // Needed for rendering in nested menus
            items: [{
                    text: 'regular item 1'
                }, {
                    text: 'regular item 2'
                }, {
                    text: 'regular item 3'
                }]
        });
