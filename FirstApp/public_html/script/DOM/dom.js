/**
 * Created by Training on 2015.02.10..
 */
function myAnimCallback() {
    console.info("Animating...");
}

Ext.onReady(
    function() {


        var spec = {
            tag: "div",
            id: "main",
            children: [
                {tag: 'div', cls: 'part', html: "Inner#1"},
                {tag: 'div', cls: 'part', html: "Inner#2"},
                {tag: 'div', cls: 'part', html: "Inner#3"},
                {id: 'anchored' , tag: 'div', cls: 'part', html: "<h5>Inner#4</h5>"}
            ]
        };
        Ext.DomHelper.append(
            Ext.getBody(),
            spec
        );

        var mainElement = Ext.get('main');
        mainElement.setStyle({
            backgroundColor: "yellow",
            border: "5px solid black",
            color: "red",
            margin: "100px"
        });
        var myAnimOptions = {
            duration: 2000,
            easing: "bounceOut",
            callback: this.myAnimCallback,
            scope: this
        };
        mainElement.setSize(200, 200, myAnimOptions);
        var parts = Ext.select(".part");
        parts.setSize(100, 100, myAnimOptions);
        parts.setStyle({
            backgroundColor: "green",
            float: "left",
            color: "yellow",
            fontSize: "bold"
        });

        var anchor = '<a href="{url}">{text}</a>';
        var myTemplate = Ext.DomHelper.createTemplate(anchor);
        myTemplate.append(
            "anchored",
            {
                url: "www.epam.com",
                text: "See more"
            }
        );
    }
);