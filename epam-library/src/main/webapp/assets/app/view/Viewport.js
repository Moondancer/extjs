/**
 *
 */

Ext.define(
    'Library.view.Viewport',
    {
        extend: 'Ext.container.Viewport',
        layout: 'fit',
        items: [
            {
                xtype: 'start'
            }
        ],
        initComponent: function() {
            console.info('Initializing EPAM Library Project Viewport...');
            var me = this;
            me.callParent();
        }
    }
);