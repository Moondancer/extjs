/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.remote;

import com.test.extlearn.domain.model.Actor;
import com.test.extlearn.service.ActorService;
import com.test.extlearn.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author HizsnyaiG
 */
@Controller
@RequestMapping("actors")
public class ActorRemote extends CrudRemote<Actor, Integer> {
    @Autowired
    private ActorService actorService;

    public void setActorService(ActorService actorService) {
        this.actorService = actorService;
    }
    
    @Override
    public CrudService<Actor, Integer> getService() {
        return this.actorService;
    }
    
}
