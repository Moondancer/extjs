package com.training360.backend.dao;

import com.training360.backend.domain.Reservation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ReservationRepository extends CrudRepository<Reservation, Long> {
    
    @Query("select reservation from Reservation reservation where reservation.book.id = :bookId and reservation.user = :user")
    public Reservation findReservationByBookIdAndUser(@Param("bookId") long bookId, @Param("user") String user);
}
