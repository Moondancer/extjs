/**
 * Created by Mark Kalinovits on 2015.02.24.
 */

Ext.define(
    'Library.view.layout.User',
    {
        extend: 'Ext.Panel',
        alias: 'widget.userlayout',

        tbar: [
            {xtype: 'textfield', itemId: 'filterField', emptyText: 'Search book here...', padding: 5}
        ],
        layout: {
            type: 'hbox',
            align: 'stretch'
        },
        items: [
            {xtype: 'booklist', padding: 10, flex: 1},
            {xtype: 'mybooklist', padding: 10, flex: 1},
        ],

        initComponent: function() {
            console.info('Initializing EPAM Library Project User layout...');
            var me = this;
            me.callParent();

            //me.getLayout().setActiveItem(1);
        }
    }
);