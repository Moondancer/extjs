/**
 * Created by Training on 2015.02.11..
 */

Ext.application(
    {
        name: 'MyApp',
        extend: 'Ext.app.Application',
        splashScreen: {},
        launch: function () {
            //MyApp.splashScreen.fadeOut({
            //    duration: 10000,
            //    remove: true
            //});
            //MyApp.splashScreen.next().fadeOut({
            //    duration: 10000,
            //    remove: true
            //});

            var task = new Ext.util.DelayedTask(function() {
                Ext.select('[role=presentation').fadeOut({
                    duration: 10000,
                    remove: true
                })

            });

            task.delay(5000);
        },
        init: function() {
            MyApp.splashScreen = Ext.getBody().mask("Loading...");
            MyApp.splashScreen.fadeIn(0);
        }
    }
);