﻿Ext.define('MovieWorld.model.Actor',{
    extend: 'Ext.data.Model',
    idPropery: 'actorId',
    fields: [
        {name: 'actorId',  type: 'int'},
        {name: 'imdb',     type: 'auto'},
        {name: 'name',     type: 'string'},
        {name: 'gender',   type: 'string'},
        {name: 'birthday', type: 'date', dateFormat: 'Y-m-d'},
        {name: 'alive',    type: 'boolean', defaultValue: true, convert: null}
    ],
    // <editor-fold defaultstate="collapsed" desc=" XML Reader ">
//    fields: [
//        {name: 'actorId',  type: 'int', mapping: '@id'},
//        {name: 'imdb',     type: 'auto'},
//        {name: 'name',     type: 'string'},
//        {name: 'gender',   type: 'string'},
//        {name: 'birthday', type: 'date', dateFormat: 'Y-m-d'},
//        {name: 'alive',    type: 'boolean', defaultValue: true, convert: null, mapping: 'live'}
//    ],
    // </editor-fold>
    validations: [
        {type: 'presence',  field: 'actorId'},
        {type: 'presence',  field: 'name'},
        {type: 'length',    field: 'name', min: 2, max: 64},
        {type: 'format',    field: 'name', matcher: /^[\w ]+$/},
        {type: 'inclusion', field: 'gender', list: ['Male', 'Female']},
        {type: 'imdb',      field: 'imdb'}
    ]
});