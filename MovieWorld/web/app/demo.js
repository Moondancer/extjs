
// <editor-fold defaultstate="collapsed" desc=" *** Helper Functions *** ">

function logActors() {
    console.log('--[ actors (',actors.count() ,') ]---------------------------------->');
    actors.each(function(model, index){
        console.log(index,
            model.get('actorId'),
            model.get('name')
        );
    });
    //console.log('--[', actors.count(), ']----------------------------------');
};

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" MODELS ">

// <editor-fold defaultstate="collapsed" desc=" Create, Get, Set ">

var actor = Ext.create("MovieWorld.model.Actor",{
    actorId: 1,
    name: 'Bill Nighy',
    gender: 'Male',
    birthday: '1949-12-12'
});
//
var name = actor.get('name');
actor.set('name', 'William Francis Nighy');
actor.set({
    imdb: '0631490',
    birthday: '1949-12-12'
});
//
console.log(actor.data.name);
console.log(actor.data.alive);
console.log(actor.data.birthday);

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Model State Information ">

console.log(actor.isModified());
console.log(actor.modified);
console.log(actor.dirty);
console.log(actor.phantom);

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Validating the Model ">

if(actor.isValid()){
    console.log('Valid!');
} else {
    var errors = actor.validate();
    errors.each(function(error){
        console.log(error.field, error.message);
    });
}

Ext.onReady(function() {
    store = Ext.create('Ext.data.Store', {
        model: 'MovieWorld.model.Actor',
        data: [{
            actorId: 1,
            name: 'Bill Nighy',
            gender: 'Male',
            birthday: '1949-12-12'
        }]
    });
    Ext.create('Ext.grid.Panel', {
        store: store,
        width: 500,
        height: 300,

        columns: [{
            text: 'imdb',
            dataIndex: 'imdb'
        }, {
            text: 'name',
            dataIndex: 'name'
        }, {
            text: 'gender',
            dataIndex: 'gender'
        }, {
            text: 'birthday',
            dataIndex: 'birthday'
        }],

        renderTo: 'grid'
    });
});

// </editor-fold>

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" MODEL RELATIONSHIPS ">

// <editor-fold defaultstate="collapsed" desc=" One-to-one (1:1) Relation ">

//Ext.define('Capital', {
//    extend: 'Ext.data.Model',
//    fields: [
//        {name: 'id', type: 'int'},
//        {name: 'capital', type: 'string'}
//    ]
//});
//
//Ext.define('Country', {
//    extend: 'Ext.data.Model',
//    fields: [
//        {name: 'id', type: 'int'},
//        {name: 'country', type: 'string'},
//        {name: 'capital_id', type: 'int'}
//    ],
//    associations: [
//        {type: 'hasOne', model: 'Capital'}
//    ]
//});

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Complex Models ">

//Ext.define('User', {
//    extend: 'Ext.data.Model',
//    fields: ['id', 'name'],
//    proxy: {
//        type: 'rest',
//        url: 'data/users',
//        reader: {
//            type: 'json',
//            root: 'users'
//        }
//    },
//    hasMany: 'Post' // shorthand for { model: 'Post', name: 'posts' }
//});
//
//Ext.define('Post', {
//    extend: 'Ext.data.Model',
//    fields: ['id', 'user_id', 'title', 'body'],
//    proxy: {
//        type: 'rest',
//        url : 'data/posts',
//        reader: {
//            type: 'json',
//            root: 'posts'
//        }
//    },
//    belongsTo: 'User',
//    hasMany: { model: 'Comment', name: 'comments' }
//});
//
//Ext.define('Comment', {
//    extend: 'Ext.data.Model',
//    fields: ['id', 'post_id', 'name', 'message'],
//    belongsTo: 'Post'
//});

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Rules for HasOne and BelongsTo ">

//Ext.define('x.y.z.model.PhoneNumber', {
//    extend: 'Ext.data.Model',
//    fields: [
//        'number',
//        'contact_id'
//    ],
//    belongsTo: [{
//        name: 'contact',
//        instanceName: 'contact',
//        model: 'x.y.z.model.Contact',
//        getterName: 'getContact',
//        setterName: 'setContact',
//        associationKey: 'contacts',
//        foreignKey: 'contact_id'
//    }],
//    proxy: {
//        type: 'ajax',
//        url: 'data/phone-numbers.json',
//        reader: {
//            type: 'json',
//            root: 'phoneNumbers'
//        }
//    }
//});

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Rules for HasMany ">

//Ext.define('x.y.z.model.Contact', {
//    extend: 'Ext.data.Model',
//    requires: [
//        'x.y.z.model.PhoneNumber'
//    ],
//    fields: [
//        'name'    // id field is inherited from Ext.data.Model
//    ],
//    hasMany: [{
//        foreignKey: 'contact_id',
//        associationKey: 'phoneNumbers',
//        name: 'phoneNumbers',
//        model: 'x.y.z.model.PhoneNumber'
//    }],
//    proxy: {
//        type: 'ajax',
//        url: 'assoc/data/contacts.json',
//        reader: {
//            type: 'json',
//            root: 'contacts'
//        }
//    }
//});

// </editor-fold>

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" STORES ">

// <editor-fold defaultstate="collapsed" desc=" Creating Store ">

//var actors = Ext.create('MovieWorld.store.Actors');
//console.log(actors.count());

//var actors = Ext.create('MovieWorld.store.Actors',{
//    extend: 'Ext.data.Store',
//    model: 'MovieWorld.model.Actor'
//});
//logActors();

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Inline Store Data ">

//var actors =  Ext.create('Ext.data.Store', {
//     model: 'MovieWorld.model.Actor',
//     data: [
//        {actorId: 1, name: 'Bill Nighy',   gender: 'Male', birthday: '1949-12-12'},
//        {actorId: 2, name: 'Keanu Reeves', gender: 'Male', birthday: '1964-09-02'}
//     ]
// });
//logActors();

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Inline Data Without Model Definition ">

//Ext.define('MyApp.store.Ratings', {
//    extend: 'Ext.data.Store',
//    fields: [
//        {name: 'label'},
//        {name: 'value', type: 'int'}
//    ],
//    data: [
//        {label: 'Great', value: 5},
//        {label: 'Above Average', value: 4},
//        {label: 'Average', value: 3},
//        {label: 'Below Average', value: 2},
//        {label: 'Poor', value: 1}
//    ]
//});


//var actors =  Ext.create('Ext.data.Store', {
//     fields: ['actorId', 'name', 'gender', 'birthday'],
//     data: [
//        {actorId: 1, name: 'Bill Nighy',   gender: 'Male', birthday: '1949-12-12'},
//        {actorId: 2, name: 'Keanu Reeves', gender: 'Male', birthday: '1964-09-02'}
//     ]
// });
//logActors();

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" ArrayStore ">

//var genreData = [
//    [1,'Action'],
//    [2,'Adventure'],
//    [3,'Comedy'],
//    [4,'Drama'],
//    [5,'Family'],
//    [6,'Fantasy']
//];
//
//var genreStore = Ext.create('Ext.data.ArrayStore',{
//    fields: [
//        {name: 'genreId', type: 'int'},
//        {name: 'genre', type: 'string'}
//    ],
//    data: genreData
//});
//console.log(genreStore.count());

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Adding New Elements ">

//var actor1 = Ext.create("MovieWorld.model.Actor",{
//    actorId: 1,
//    name: 'Bill Nighy',
//    gender: 'Male',
//    birthday: '1949-12-12'
//});
//
//var actor2 = Ext.create("MovieWorld.model.Actor",{
//    actorId: 2,
//    name: 'Keanu Reeves',
//    gender: 'Male',
//    birthday: '1964-09-02'
//});
//
//actors.add(actor1, actor2);
//console.log(actors.count());

//actors.add([actor1, actor2]);
//console.log(actors.count());

//actors.add({
//    actorId: 3,
//    name: 'Mads Mikkelsen',
//    gender: 'Male',
//    birthday: '1965-11-22'
//});

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Inserting New Elements ">

//actors.insert(0,
//    [{
//        actorId: 4,
//        name: 'Laurence Fishburne',
//        gender: 'Male',
//        birthday: '1961-07-30'
//    },
//    {
//        actorId: 5,
//        name: 'Matt Damon',
//        gender: 'Male',
//        birthday: '1970-10-08'
//    }]
//);

//console.log(actors.count());

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Iterating Through the Elements ">

/*actors.each(function(model, index){
    console.log(index,
        model.get('actorId'),
        model.get('name')
    );
});*/

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Retrieving Elements ">

/*console.log(actors.getAt(2).get('name')); //Laurence Fishburne
console.log(actors.first().get('name')); //Bill Nighy
console.log(actors.last().get('name')); //Matt Damon

var list = actors.getRange(1,3);
Ext.each(list, function(model,index){
    console.log(index, model.get('name'));
});*/

//console.log(actors.getById(4));
//console.log(actors.findRecord('actorId',4).get('name'));

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Removing Elements ">

//logActors();

//actors.remove(actor1);
//actors.remove([actor1, actor2]);
//actors.remove([0,3,4]);

//actors.removeAt(0);
//actors.removeAt(1,3);

//actors.removeAll();

//logActors();

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Sorting Data ">

//logActors();

//Sort by a single field
//actors.sort('name');
//logActors();
//
//actors.sort('name');
//logActors();
//
//actors.sort('name', 'DESC');
//logActors();

//Sorting by multiple fields
//actors.sort([{
//    property : 'birthday',
//    direction: 'DESC'
//}, {
//    property : 'name',
//    direction: 'ASC'
//}]);

// Sort the store using the existing sorter set
//actors.sort();
//
//logActors();

// </editor-fold>

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" AJAX ">

// <editor-fold defaultstate="collapsed" desc=" AJAX Request ">

//Ext.Ajax.request({
//    url : "data/example1.json",
//    success : function(response,options){
//        var data = Ext.decode(response.responseText);
//        Ext.Msg.alert("Message", data.msg);
//    },
//    failure : function(response,options){
//        //...
//    }
//});

// </editor-fold>

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" PROXIES ">

// <editor-fold defaultstate="collapsed" desc=" Mappings ">

//var actors = Ext.create("MovieWorld.store.Actors");
//logActors();

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" Array Reader ">

//var actors = Ext.create("MovieWorld.store.Actors");
//actors.load(function(){
//    logActors();
//});

// </editor-fold>

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" LOADING AND SYNCHRONIZING STORES ">

// <editor-fold defaultstate="collapsed" desc=" autoLoad - Loading Store Data Automatically ">

//var actors = Ext.create("MovieWorld.store.Actors", {
//    listeners: {
//       load: function(store){
//           logActors();
//       }
//    }
//});

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc=" load() - Loading Store Data Manually ">

//var actors = Ext.create("MovieWorld.store.Actors");
//
//actors.load(function(){
//    console.log('Data loaded');
//});
//
//actors.load({
//    scope: this,
//    callback: function(records, operation, success) {
//        console.log(success);
//        if (success) console.log(records.length);
//    }
//});

// </editor-fold>

// </editor-fold>