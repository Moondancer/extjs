/**
 * EPAM ExtJS Library Project
 *
 * author: Mark Kalinovits
 * since: 23-02-2015
 */

Ext.application(
    {
        name: 'Library',
        appFolder: 'assets/app',
        autoCreateViewport: true,

        requires: ['Library.Config'],
        models: [
            'Book',
            //'Reservation'
        ],
        stores: [
            'Books',
            'Reservations',
            'ReservedBooks',
            'MyBooks'
        ],
        views: [
            'window.Login',
            'window.Reservation',

            'Start',
            'layout.Admin',
            'layout.User',

            'grid.Books',
            'grid.ReservedBooks',
            'grid.Reservations',

            'list.Books',
            'list.MyBooks',

            'form.Book',
            'form.Reservation'
        ],
        controllers: [
            'Login',
            'Start',
            'Books',
            'Reservations',
            'Borrow'
        ],

        init: function() {
            console.info('Initializing EPAM Library Project...');
            Library.Config.setUser(user_user);
            Library.Config.setRole(user_role.replace('&#91;','').replace('&#93;','').replace('&#95;','_'));
            delete user_user;
            delete user_role;
        },

        launch: function() {
            console.info('EPAM Library Project is launching...');

            // Code here

            console.info('EPAM Library Project is launched...');
        }
    }
);
