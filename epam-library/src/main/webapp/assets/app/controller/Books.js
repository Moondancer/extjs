/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define(
    'Library.controller.Books',
    {
        extend: 'Ext.app.Controller',
        views: ['grid.Books', 'form.Book'],
        models: ['Book'],

        refs: [
            {ref: 'grid', selector: 'booksgrid'},
            {ref: 'form', selector: 'bookform'},
        ],

        init: function() {
            console.info('Initializing EPAM Library Project Books controller...');
            var me = this;
            me.callParent();

            me.task = new Ext.util.DelayedTask(); //For filtering

            me.control(
                {
                    'booksgrid':                {itemclick:     me.onItemClick},
                    'booksgrid #filterField':   {change:        me.onBookFilter},
                    'bookform':                 {render:        me.onLoad},
                    'bookform #btnNew':         {click:         me.onNew},
                    'bookform #btnSave':        {click:         me.onSave},
                    'bookform #btnDelete':      {click:         me.onDelete}
                }
            );
        },

        onItemClick: function(grid, record) {
            console.info('Books controller onItemClick action');
            var me = this;
            this.getForm().loadRecord(record);
            me.getForm().down('#btnDelete').enable();
        },
        onNew: function (btn) {
            console.info('Books controller onNew action');
            var me = this;
            me.getForm().loadRecord(this.getBookModel().create());
            me.getForm().down('#btnDelete').disable();
        },
        onSave: function (btn) {
            console.info('Books controller onSave action');
            var me = this;
            var form = me.getForm();

            var rec = form.getRecord();

            if (rec) {
                var type = '';
                rec.set(form.getForm().getFieldValues());
                if (rec.phantom) {
                    me.getGrid().store.add(rec);
                    type = 'new';
                    me.getGrid().store.sync({
                        success: function() {me.msgSuccess(type);}
                    });
                } else {
                    type = 'this';
                    me.getGrid().store.sync({
                            success: function() {me.msgSuccess(type);}
                    });
                }
                me.getForm().getForm().reset();
            }
        },
        onDelete: function(btn) {
            var me = this;
            var grid = me.getGrid();
            var model = me.getForm().getRecord();
            Ext.MessageBox.confirm('', 'Do you want to delet this record?', function (btn, text) {
                if (btn == 'yes') {
                    grid.store.remove(model);
                    me.getGrid().store.sync({
                        success: function() {
                            Ext.Msg.alert('', 'The book has been deleted successfully.');
                            me.getForm().getForm().reset();
                        }
                    });

                }
            }, me);

        },
        onLoad: function() {
            console.info('Books controller Render action');
            var me = this;
            me.onNew();
        },
        onBookFilter: function() {
            console.info('Books controller onBookFilter action');
            var me = this;

            me.task.delay(
                500,
                function () {
                    var store;
                    var field;

                    field = this.getGrid().down('#filterField');
                    store = this.getGrid().getStore();
                    store.getProxy().extraParams = {
                        query: field.value
                    };
                    store.load();
                },
                me
            );
        },
        msgSuccess: function(type) {
            type = type.substr(0, 1).toUpperCase() + type.substr(1);
            Ext.MessageBox.alert('', type + ' book has successfully saved');
        }
    }
);
