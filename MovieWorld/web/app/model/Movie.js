﻿Ext.define('MovieWorld.model.Movie',{
    extend: 'Ext.data.Model',
    idPropery: 'movieId',
    fields: [
        {name: 'movieId', type: 'int'},
        {name: 'imdb',    type: 'auto'},
        {name: 'title',   type: 'string'},
        {name: 'year',    type: 'int'}
    ],
    validations: [
        {type: 'presence',  field: 'movieId'},
        {type: 'presence',  field: 'title'},
        {type: 'imdb',      field: 'imdb'},
        {type: 'year',      field: 'year'}
    ]
});