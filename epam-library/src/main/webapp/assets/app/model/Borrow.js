/**
 * Created by Mark Kalinovits on 2015.02.26..
 */

Ext.define(
    'Library.model.Borrow',
    {
        extend: 'Ext.data.Model',
        requires: ['Library.Config'],

        fields: [
            {name: 'bookId', type: 'int'},
            {name: 'user', type: 'string'}
        ],
        proxy: {
            type: 'rest',
            url: 'http://localhost:8080/api/books/',
            reader: {
                type: 'json',
                root: 'items',
                totalProperty: 'totalCount'
            },
            buildUrl: function(request) {
                var me = this;
                var data = request.records[0].data;

                var operation = request.operation;
                var records   = operation.records || [];
                var record    = records[0];
                var url       = me.getUrl(request);
                var user      = mcnf.getUser();;
                var bookId    = data.bookId;

                console.log('Request action: ' + request.action);

                switch (request.action) {
                    case 'read':
                        url += 'mybooks/' + user;
                        console.log(url);
                        break;
                    case 'create':
                        url += bookId + '/borrow/' + user;
                        console.log(url);
                        break;
                    case 'delete':
                        url += bookId + '/borrow';
                        console.log(url);
                        break;
                    case 'update':
                        url += bookId + '/borrow/' + user;
                        console.log(url);
                        break;
                    default:
                        throw new Error("No such request action!");
                        break;
                }
                return url;
            }
        },



        initComponent : function() {
            console.info('Initializing EPAM Library Project Book model...');
            var me = this;
            me.callParent();

            me.addEvents('bookException');
        }
    }
);
