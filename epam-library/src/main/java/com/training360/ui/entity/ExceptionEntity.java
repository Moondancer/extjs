package com.training360.ui.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wordnik.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

@ApiModel
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExceptionEntity {
    
    private String msg;
    
    private Map<String, String> errors;

    public ExceptionEntity(String msg, Map<String, String> errors) {
        this.msg = msg;
        this.errors = errors;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public boolean isSuccess() {
        return false;
    }
    
    public int getTotal() {
        return 0;
        
    }
}
