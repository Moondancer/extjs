package com.training360.ui.transformer;

import com.training360.backend.domain.Book;
import com.training360.backend.domain.Reservation;
import com.training360.ui.entity.BookResponseItem;
import com.training360.ui.entity.BooksResponse;
import com.training360.ui.entity.ReservationItem;
import org.springframework.data.domain.Page;

import java.util.ArrayList;

public class BooksResponseTransformer {
    
    public BooksResponse transform(Page<Book> books, boolean withReservations) {
        BooksResponse booksResponse = new BooksResponse();
        for (Book book: books) {
            BookResponseItem bookResponseItem = transformItem(book, withReservations);
            booksResponse.getItems().add(bookResponseItem);
        }
        booksResponse.setTotalCount(books.getTotalElements());
        return booksResponse;
    }

    public BooksResponse transform(Book book, boolean withReservations) {
        BooksResponse booksResponse = new BooksResponse();
        BookResponseItem bookResponseItem = transformItem(book, withReservations);
        booksResponse.getItems().add(bookResponseItem);
        return booksResponse;
    }

    private BookResponseItem transformItem(Book book, boolean withReservations) {
        BookResponseItem bookResponseItem = new BookResponseItem();
        bookResponseItem.setId(book.getId());
        bookResponseItem.setAuthor(book.getAuthor());
        bookResponseItem.setTitle(book.getTitle());
        bookResponseItem.setBorrowedBy(book.getBorrowedBy());
        if (withReservations) {
            bookResponseItem.setReservationItems(new ArrayList<ReservationItem>(book.getReservations().size()));
            for (Reservation reservation: book.getReservations()) {
                ReservationItem reservationItem = new ReservationTransformer().transform(reservation);
                bookResponseItem.getReservationItems().add(reservationItem);
            }
        }
        return bookResponseItem;
    }
}
