/**
 * Created by Training on 2015.02.09..
 */
Ext.define(
    'FirstApp.script.ooBasics.Teacher',
    {
        extend: 'FirstApp.script.ooBasics.User',
        constructor: function(args) {
            this.initConfig(args);
            this.callParent();
        },
        MAX_CLASS_SIZE: 40,
        config: {
            Class: null,
            TeacherID: null
        },
        EnrollInClass: function(){},
        RemoveFromClass: function(){},
        ValidatePassword: function(password){
            var result = false;
            if(typeof password == "string" &&
                password.length >= 8 &&
                password.match(/\d/g) != null &&
                password.match(/\d/g).length >= 2) {
                result = true;
            }

            return result;
        }
    }
);
