/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.remote;

import com.test.extlearn.domain.model.Role;
import com.test.extlearn.service.CrudService;
import com.test.extlearn.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author HizsnyaiG
 */
@Controller
@RequestMapping("roles")
public class RoleRemote extends CrudRemote<Role, Integer> {
    @Autowired
    private RoleService roleService;

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public CrudService<Role, Integer> getService() {
        return this.roleService;
    }
}
