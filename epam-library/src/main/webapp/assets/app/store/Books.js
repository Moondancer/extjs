/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define(
    'Library.store.Books',
    {
        extend: 'Ext.data.Store',
        model: 'Library.model.Book',
        //autoSync: true,
        autoLoad: true,
        remoteSort: true,
        remoteFilter: true,
        proxy: {
            type: 'rest',
            url: 'http://localhost:8080/api/books/',
            reader: {
                type:           'json',
                root:           'items',
                totalProperty:  'totalCount'
            },
            listeners: {
                exception: function(proxy, response, options) {
                    console.info('Book model proxy Exception listener');
                    var responseObject = JSON.parse(response.responseText);
                    Ext.Msg.alert('Error', responseObject.msg);
                }
            }
        },

        initComponent: function() {
            console.info('Initializing EPAM Library Project StoreBooks...');
            var me = this;
            me.callParent();
        }
    }
);
