/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define(
    'Library.store.Reservations',
    {
        extend: 'Ext.data.Store',
        model: 'Library.model.Reservation',
        autoSync: true,
        data: [],

        initComponent: function() {
            console.info('Initializing EPAM Library Project Reservation store...');
            var me = this;
            me.callParent();
        }
    }
);
