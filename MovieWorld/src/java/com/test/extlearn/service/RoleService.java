/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import com.test.extlearn.domain.model.Role;
import java.util.List;

/**
 *
 * @author HizsnyaiG
 */
public interface RoleService extends CrudService<Role, Integer> {

}
