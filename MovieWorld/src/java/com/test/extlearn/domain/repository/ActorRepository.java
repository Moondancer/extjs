/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.domain.repository;

import com.test.extlearn.domain.model.Actor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author HizsnyaiG
 */
public interface ActorRepository extends CrudRepository<Actor, Integer> {
    
    public Page<Actor> findAll(Pageable p);
}
