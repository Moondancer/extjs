/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define(
    'Library.view.grid.Reservations',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.reservationsgrid',

        store: 'Reservations',

        plugins: {
          ptype: 'cellediting',
            clicksToEdit: 2
        },
        columns: [
            {
                text: 'User',
                dataIndex: 'user',
                flex: 3
            },
            {
                text: 'Deadline',
                dataIndex: 'deadline',
                flex: 4,
                editor: 'datefield'
            },
            {
                text: 'Action',
                xtype:'actioncolumn',
                flex: 1,
                items: [
                    {
                        cls: 'x-tool x-tool-close',
                        icon: '/assets/resources/images/Cancel.png',  // Use a URL in the icon config
                        tooltip: 'Cancel',
                        handler: function(grid2, rowIndex, colIndex) {
                            var rec = grid2.getStore().getAt(rowIndex);
                            var grid = Ext.ComponentQuery.query('reservationsgrid');
                            grid[0].fireEvent('cancelReservation', rec);
                        }
                    }
                ]
            }
        ],
        initComponent: function() {
            console.info('Initializing EPAM Library Project Reservations grid view...');
            var me = this;
            me.callParent(arguments);

            this.addEvents('editReservation', 'cancelReservation');
        }
    }
);