/**
 * Created by Training on 2015.02.10..
 */

// Data
var data = [
    {
        name: 'James Bond',
        year: 1913,
        profession: 'Agent',
        company: 'MI6',
        hobbies: ['chasing bad guys', 'love Bond girls', 'driving Aston Martin DB5', "using Q's gadgets"],
        kids: []
    },
    {
        name: 'Jane Shy',
        year: 1971,
        profession: 'Sales manager',
        company: 'EPAM',
        hobbies: ['tennis', 'cinema', 'driving'],
        kids: [{name:'Emma',year:2010},
            {name:'Aaron',year:2000}]
    }
];

// Templetes
var personTemplate = new Ext.XTemplate(
    '<tpl for=".">' +
    '<div class="person">' +
        '<h2>{name}</h2>' +
        '<h3>{profession} at {company}</h3>' +
        '<p>Age: {[this.getAge(values.year)]}</p>' +
        '<h4>Hobby</h4>' +
        '<ul>' +
            '<tpl for="hobbies">' +
            '<li>{.}</li>' +
            '</tpl>' +
        '</ul>' +
        '<h4>Kids</h4>' +
            '<tpl for="kids">' +
            '<li>{name} ({[this.getAge(values.year)]})</li>' +
            '</tpl>' +
        '</ul>' +
    '</div>' +
    '</tpl>',
    {
        getAge: function(year) {
            return new Date().getFullYear() - year;
        }
    }
);

// Filters
var employerFilter = new Ext.util.Filter({
    property: 'company',
    value: 'EPAM'
});

// Program
Ext.onReady(
    function() {
        Ext.create(
            'Ext.panel.Panel',
            {
                title: "My panel",
                renderTo: Ext.getBody(),
                dockedItems: [
                    {
                        xtype: 'toolbar',
                        dock: 'top',
                        items: [
                            {text: 'Print'},
                            {text: 'Print'},
                            {text: 'Print'},
                            {text: 'Print'}
                        ]
                    }
                ]
            }
        );

        Ext.DomHelper.append(
            Ext.getBody(),
            {
                tag: 'div',
                id: 'main'
            }
        );

        personTemplate.overwrite('main', data);

        var prompt = Ext.Msg.prompt('Company', 'Enter company name to list employee', function(btn, company){
            if(btn = "ok") {
                var filtered = data.filter(employerFilter); //????
                Ext.Msg.alert(
                    'Employees of ' + company,
                    filtered
                );
            }
        })
    }
);