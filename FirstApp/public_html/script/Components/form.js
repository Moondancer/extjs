/**
 * Created by Training on 2015.02.11..
 */
Ext.onReady(
    function() {
        Ext.create('Ext.form.Panel', {
            title: 'Simple Form',
            bodyPadding: 5,
            width: 350,

            // The form will submit an AJAX request to this URL when submitted
            //url: 'save-form.php',

            // Fields will be arranged vertically, stretched to full width
            layout: 'anchor',
            defaults: {
                anchor: '100%'
            },

            // The fields
            defaultType: 'textfield',
            items: [{
                fieldLabel: 'First Name',
                name: 'first',
                allowBlank: false
            },{
                fieldLabel: 'Last Name',
                name: 'last',
                allowBlank: false
            },{
                xtype: 'numberfield',
                name: "year",
                fieldLabel: "Year of Birth",
                allowDecimals: false,
                minValue: 1900,
                maxValue: new Date().getFullYear(),
                enableKeyEvents: true,
                listeners: {
                    keypress: function(t, e) {
                        var dot = ".";
                        if(e.getKey() === dot.charCodeAt(0)) {
                            e.stopEvent();
                        }
                    }
                }
            },{
                xtype: "datefield",
                name: "date",
                fieldLabel: "Date",
                format: "Y/m/d",
                altFormats: "Y.m.d|Y m d",
                minValue: new Date(),
                disabledDates: ["2017/../..", "03/13"]
            },{
                xtype: "checkbox",
                fieldLabel: "No",
                name: "no"
            },{
                xtype: "checkbox",
                fieldLabel: "",
                name: "yes",
                boxLabel: "Yes"
            },{
                xtype: "checkboxgroup",
                fieldLabel: "Sports",
                defaultType: "checkbox",
                columns: 2,
                vertical: true,
                items: [
                    {name: "sp", boxLabel: "Tennis", inputValue: "1"},
                    {name: "sp", boxLabel: "Golf", inputValue: "2"},
                    {name: "sp", boxLabel: "Socker", inputValue: "3"},
                    {name: "sp", boxLabel: "Swimming", inputValue: "4"}
                ]
            }],

            // Reset and Submit buttons
            buttons: [{
                text: 'Reset',
                handler: function() {
                    this.up('form').getForm().reset();
                }
            }, {
                text: 'Submit',
                formBind: true, //only enabled once the form is valid
                disabled: true,
                handler: function() {
                    var form = this.up('form').getForm();
                    if (form.isValid()) {
                        //Ext.Msg.alert("Validation", "Form is valid: " + form.isValid());
                        saveData(form);
                        //form.submit({
                        //    success: function(form, action) {
                        //        Ext.Msg.alert('Success', action.result.msg);
                        //    },
                        //    failure: function(form, action) {
                        //        Ext.Msg.alert('Failed', action.result.msg);
                        //    }
                        //});
                    }
                }
            }],
            renderTo: Ext.getBody()
        });
        function saveData(form) {
            Ext.Msg.alert("Save", "<p>Form is valid: " + form.isValid() + "</p>" +
            "<p>Values: " + form.getValues().sp + "</p>");
        }
    }
);