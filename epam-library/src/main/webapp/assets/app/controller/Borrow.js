/**
 * Created by Mark Kalinovits on 2015.02.26..
 */

Ext.define(
    'Library.controller.Borrow',
    {
        extend: 'Ext.app.Controller',
        requires: ['Library.Config'],

        views: [
            'list.Books',
            'list.MyBooks'
        ],
        models: [
            'Borrow'
        ],

        refs: [
            {ref: 'bookList', selector: 'booklist'},
            {ref: 'myBookList', selector: 'mybooklist'},
            {ref: 'reservationForm', selector: 'reservationform'}
        ],

        init: function() {
            console.info('Initializing EPAM Library Project Borrow controller...');
            var me = this;
            me.callParent();

            me.control(
                {
                    'booklist': {
                        select:         me.onSelect,
                        itemdblclick:   me.onBookItemDoubleClick
                    },
                    'mybooklist': {
                        itemdblclick:   me.onMyBookDoubleClick
                    },
                    'reservationform': {
                        submitForm:          me.onSubmitForm
                    }
                }
            );
        },
        onSelect: function(object, record, eOpts) {
            console.info('Borrow controller onSelect action...');

        },
        onBookItemDoubleClick: function(listView, record, item, index, e, eOpts ) {
            e.preventDefault();
            console.info('Borrow controller onBookItemDoubleClick action...');
            var controller = this;
            var targetStore = controller.getMyBookList().getStore();
            var borrowModel = Ext.create('Library.model.Borrow');

            if(!record.get('borrowedBy')) {
                console.info('Borrowing book');

                borrowModel.set('bookId', record.get('id'));
                borrowModel.save();

                targetStore.add(record);
            } else {
                Ext.create('Library.view.window.Reservation',
                    {edit: false, bookId: record.get('id'), user: mcnf.getUser()}
                ).show();
            }
            controller.getBookList().getStore().reload();
        },
        onSubmitForm: function(data, id, edit) {
            console.info('Borrow controller onSubmitForm action...');

            var me = this;
            var targetStore = me.getMyBookList().getStore();

            me.getReservationForm().up('window').close();

            var reservationModel = Ext.create('Library.model.MyReservation', data);
            reservationModel.proxy.setUrl(id, mcnf.getUser());
            reservationModel.save();
        },
        onMyBookDoubleClick: function(listView, record, item, index, e, eOpts) {
            e.preventDefault();
            console.info('Borrow controller onMyBookDoubleClick action...');
            Ext.create('Library.view.window.Reservation',
                {edit: true, bookId: record.get('id'), user: mcnf.getUser()}
            ).show();
        }

    }
);
