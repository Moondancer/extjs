
var data = [{
    name: 'James Bond',
    profession: 'Agent',
    company: 'MI6',
    hobbies: ['chasing bad guys', 'love Bond girls', 'driving Aston Martin DB5', "using Q's gadgets"],
    kids: []
},
{
    name: 'Jane Shy',
    profession: 'Sales manager',
    company: 'EPAM',
    hobbies: ['tennis', 'cinema', 'driving'],
    kids: [{name:'Emma',year:2010},
    {name:'Aaron',year:2000}]
}];
