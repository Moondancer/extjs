/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.text.SimpleDateFormat;

/**
 *
 * @author HizsnyaiG
 */
public class CustomJacksonMapper extends ObjectMapper {
    
    public CustomJacksonMapper() {
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);            
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);            
        setDateFormat(new SimpleDateFormat("MM-dd-yyyy"));
    }
}
