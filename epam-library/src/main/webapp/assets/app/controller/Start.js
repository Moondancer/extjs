/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define(
    'Library.controller.Start',
    {
        extend: 'Ext.app.Controller',
        views: ['Start'],
        models: [],

        refs: [
            {ref: 'start',      selector: 'start'},
            {ref: 'admin',      selector: 'start #adminMenu'},
            {ref: 'borrower',   selector: 'start #userMenu'}
        ],

        init: function() {
            console.info('Initializing EPAM Library Project MainMenu controller...');
            var me = this;
            me.callParent();

            me.control(
                {
                    'start #adminMenu':    {click: me.loadAdmin},
                    'start #userMenu':     {click: me.loadUser}
                }
            );
        },

        loadAdmin: function() {
            console.info('Start controller loadAdmin action');
            var me = this;
            me.getStart().loadAdmin();
        },
        loadUser: function() {
            console.info('Start controller loadUser action');
            var me = this;
            me.getStart().loadUser();
        }
    }
);
