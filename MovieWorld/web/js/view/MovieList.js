Ext.define(
        'MovieWorld.view.MovieList',
        {
            alias: 'widget.movielist',
            extend: 'Ext.view.View',
            store: 'MovieStore',
            tpl: new Ext.XTemplate(
                    '<tpl for=".">' +
                    '<div class="wrap">' +
                    '<b>{title}</b><span>{imdb}</span>' +
                    '</div>' +
                    '</tpl>'
                    ),
            itemSelector: 'div.wrap'
        }
        );
