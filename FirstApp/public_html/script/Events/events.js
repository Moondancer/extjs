/**
 * Created by Training on 2015.02.10..
 */

function hideSg() {
    this.hide();
    Ext.defer(function() {
       this.show();
    }, 2000, this);
}

Ext.onReady(
    function() {
        var btn = Ext.create(
            'Ext.button.Button',
            {
                text: "Don't click me!",
                renderTo: Ext.getBody(),
                scale: 'large'
            }
        );

        // Events
        btn.on('click', function() {console.warn("Don't touch me!")});
        btn.addListener('click', function(){Ext.Msg.alert("PANIC...", "Don't touch me!")})

        var btn2 = Ext.create(
            'Ext.button.Button',
            {
                text: "Click me!",
                renderTo: Ext.getBody(),
                scale: 'large',
                listeners: {
                    click: function() {
                        this.setText("You clicked me.");
                    },
                    mouseover: function() {
                        this.setText("Please click me!");
                    },
                    mouseout: function() {
                        this.setText("Click me!");
                    }
                }
            }
        );

        var btn3 = Ext.create(
            'Ext.button.Button',
            {
                text: "Hide me!",
                renderTo: Ext.getBody(),
                scale: 'large',
                listeners: {
                    click: hideSg
                }
            }
        );

        var panel = Ext.create(
            'Ext.panel.Panel',
            {
                title: 'Panel',
                renderTo: Ext.getBody()
            }
        );

        var btn4 = Ext.create(
            'Ext.button.Button',
            {
                text: "I'd like to hide others.",
                renderTo: Ext.getBody(),
                scale: 'large',
                listeners: {
                    click: {
                        fn: hideSg,
                        scope: panel
                    }
                }
            }
        );

        // Menu button
        var btn5 = Ext.create(
            'Ext.button.Button',
            {
                text: "I have menus!",
                renderTo: Ext.getBody(),
                scale: 'large',
                menu: [
                    {text: "1"},
                    {text: "2"}
                ]
            }
        );

        // Context menu
        var cMenu= Ext.create("Ext.menu.Menu", {
                width:100,
                floating: true,
                items: [
                    {text: "menu 1"},
                    {text: "menu 2"}
                ]
        });

        var btn6 = Ext.create("Ext.button.Button", {
            text: "Try my contextmenu",
            renderTo: Ext.getBody(),
            scale: 'large'
        });

        btn6.getEl().on('contextmenu', function(e) {
            e.preventDefault();
            cMenu.showAt(e.getXY());
        });

        // Warning on close
        Ext.EventManager.on(
            window,
            'beforeunload',
            function(e) {
                e.stopEvent();
                if(e.browserEvent){
                    e.browserEvent.returnValue = "WARNING!";
                }
            }
        );
    }
);