/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('MovieWorld.view.ActorGrid', {
    extend: 'Ext.grid.Panel',
    store: Ext.create('MovieWorld.store.ActorStore'),
    
    alias: 'widget.actorgrid',
    
    tbar: [
        'Filter: ',
        {xtype: 'textfield', itemId: 'filterField'},
        {text: "Menu", menu: {xtype: 'mainmenu'}}

    ],

    columns: [{
            text: 'imdb',
            dataIndex: 'imdb'
        }, {
            text: 'name',
            dataIndex: 'name',
            renderer: function (v) {
                return '<strong>' + v + '</strong>';
            },
            flex: 1
        }, {
            text: 'gender',
            dataIndex: 'gender'
        }, {
            text: 'birthday',
            dataIndex: 'birthday',
            xtype: 'datecolumn'
        }, {
            text: 'alive',
            dataIndex: 'alive',
            xtype: 'checkcolumn',
        }
    ],
    dockedItems: [
        {
            xtype: 'pagingtoolbar',
            store: 'ActorStore',
            dock: 'bottom'
        }
    ]
});
