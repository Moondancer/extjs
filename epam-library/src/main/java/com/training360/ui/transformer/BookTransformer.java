package com.training360.ui.transformer;

import com.training360.backend.domain.Book;
import com.training360.ui.entity.BookRequest;

public class BookTransformer {
    
    public Book transform(BookRequest bookRequest) {
        Book book = new Book();
        book.setAuthor(bookRequest.getAuthor());
        book.setTitle(bookRequest.getTitle());
        return book;
    }
}
