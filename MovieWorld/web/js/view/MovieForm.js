Ext.define('MovieWorld.view.MovieForm', {
    extend: 'Ext.form.Panel',
    modal: true,
    title: 'Movie details',
    width: 300,
    height: 400,
    layout: 'fit',
    alias: 'widget.movieform',
    tbar: [{
            text: 'New',
            itemId: 'btnNew'
        }, '-', {
            text: 'Remove',
            itemId: 'btnRemove',
            disabled: true
        }, '->', {
            text: 'Save',
            itemId: 'btnSave',
            disabled: true,
            formBind: true,
        }
    ],
    items: [{
            xtype: 'form',
            bodyStyle: 'padding:5px',
            items: [
                {
                    name: 'movieId',
                    fieldLabel: "Id",
                    xtype: 'displayfield'
                }, {
                    name: 'imdb',
                    fieldLabel: 'IMDB',
                    xtype: 'textfield'
                }, {
                    name: 'title',
                    fieldLabel: 'Title',
                    xtype: 'textfield',
                    allowBlank: false
                }, {
                    name: 'year',
                    fieldLabel: 'Premier',
                    xtype: 'numberfield',
                    minValue: 1890,
                }]
        }],
    initComponent: function () {
        this.callParent();
        this.addEvents('change');
    },
//    buttons: [{
//            text: 'OK',
//            itemId: 'btnOk'
//        }, {
//            text: 'Cancel',
//            itemId: 'btnCancel'
//        }]
});

