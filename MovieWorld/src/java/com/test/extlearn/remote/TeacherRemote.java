/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.remote;

import com.test.extlearn.domain.model.Teacher;
import com.test.extlearn.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author HizsnyaiG
 */
@Controller
@RequestMapping("teachers")
public class TeacherRemote extends CrudRemote<Teacher, Integer> {
    @Autowired
    private TeacherService teacherService;

    public void setTeacherService(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @Override
    public TeacherService getService() {
        return this.teacherService;
    }
    
}
