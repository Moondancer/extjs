﻿Ext.define('MovieWorld.store.Actors',{
    extend: 'Ext.data.Store',
    model: 'MovieWorld.model.Actor',
    
    // <editor-fold defaultstate="collapsed" desc=" Loading Store Data Automatically ">
    //autoLoad: true,
    autoLoad: {
        scope: this,
        callback: function(records, operation, success) {
            console.log(success);
        }
    },
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" JSON Reader ">
    proxy: {
        type: 'ajax',
        url: 'data/actors.json',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" XML Reader ">    
//    proxy: {
//        type: 'ajax',
//        url: 'data/actors.xml',
//        reader: {
//            type: 'xml',
//            root: 'actors',
//            record: 'actor'
//        }
//    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Array Reader ">    
//    proxy: {
//        type: 'ajax',
//        url: 'data/actors-array.json',
//        reader: {
//            type: 'array'
//        }
//    }
    // </editor-fold>
    
});