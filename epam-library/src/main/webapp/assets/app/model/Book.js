/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define(
    'Library.model.Book',
    {
        extend: 'Ext.data.Model',
        requires: ['Library.model.Reservation'],
        itemId: 'bookmodel',

        idProperty: 'id',
        fields: [
            {name: 'id'},
            {name: 'title', type: 'string'},
            {name: 'author', type: 'string'},
            {name: 'borrowedBy', type: 'string'}
        ],
        hasMany: {
            model: 'Library.model.Reservation',
            name: 'reservationItems'
        },
        proxy: {
            type: 'rest',
            url: 'http://localhost:8080/api/books/',
            reader: {
                type: 'json',
                root: 'items',
                totalProperty: 'totalCount'
            }
        },

        initComponent : function() {
            console.info('Initializing EPAM Library Project Book model...');
            var me = this;
            me.callParent();

            me.addEvents('bookException');
        }
    }
);
