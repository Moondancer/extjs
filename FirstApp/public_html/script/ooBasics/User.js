/**
 * Created by Training on 2015.02.09..
 */

Ext.define(
    'FirstApp.script.ooBasics.User',
    {
        constructor: function(args) {
            this.initConfig(args);
        },
        config: {
            FirstName: "Dude",
            LastName: "Dude",
            Password: "Password",
            UserName: "Dude"
        },
        statics: {
            autoID: 0,
            getNextID: function() {
                return ++this.autoID;
            }
        },
        ValidatePassword: function(password) {return true;},
        VerifyPassword: function(password) {},
        applyPassword: function(newPassword, oldpassword) {
            var valid = this.ValidatePassword(newPassword);
            if(!valid) {
                console.error("Invalid password! " + newPassword);
            }
        }
    }
);