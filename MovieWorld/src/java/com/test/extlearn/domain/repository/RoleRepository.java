/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.domain.repository;

import com.test.extlearn.domain.model.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author HizsnyaiG
 */
public interface RoleRepository extends CrudRepository<Role, Integer> {
    
    public Page<Role> findAll(Pageable p);
    
    public Page<Role> findByActorId(Integer actorId, Pageable p);
    public Long countByActorId(Integer actorId);
    
    public Page<Role> findByMovieId(Integer movieId, Pageable p);
    public Long countByMovieId(Integer movieId);
}
