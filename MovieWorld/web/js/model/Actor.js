/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('MovieWorld.model.Actor', {
    extend: 'Ext.data.Model',
    idProperty: 'actorId',
    fields: [
        {name: 'actorId'},
        {name: 'imdb'},
        {name: 'name'},
        {name: 'gender'},
        {
            name: 'birthday',
            type: 'date',
        }, {
            name: 'alive',
            type: 'boolean',
        }
    ],
    hasMany: {
        model: 'MovieWorld.model.Role',
        name: 'roleList'
    },

//    validations: [
//        {type: 'presence', field: 'name'},
//        {type: 'length', field: 'name', min: 2},
//        {type: 'inclusion', field: 'gender', list: ['male', 'female']},
//    ],

//    proxy: {
//        type: 'rest',
//        url: 'service/actors',
//        reader: {
//            type: 'json',
//            root: 'data'
//        }
//    }
//    proxy: {
//        type: 'ajax',
//        url: 'data/actors-array.json', // Static file at data folder
//        reader: {
//            type: 'array'
//        }
//    }
    proxy: {
        type: 'ajax',
        url: 'data/actor-role.json', // Static file at data folder
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
