/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define('Library.view.form.Book', {
    extend: 'Ext.form.Panel',
    modal: true,
    //title: 'Book details',
    width: 300,
    height: 400,
    layout: 'fit',

    alias: 'widget.bookform',
    tbar: [
    {
        text: 'New',
        itemId: 'btnNew',
        flex:1
    },
    //'-',
    {
        text: 'Save',
        itemId: 'btnSave',
        disabled: true,
        formBind: true,
        flex:1
    },
    //'->',
    {
        text: 'Delete',
        itemId: 'btnDelete',
        disabled: true,
        flex:1
    }
    ],
    items: [{
        xtype: 'form',
        bodyStyle: 'padding:5px',
        items: [
            {
                name: 'id',
                fieldLabel: "Book ID",
                xtype: 'displayfield'
            }, {
                name: 'title',
                fieldLabel: 'Title',
                xtype: 'textfield',
                allowBlank: false
            }, {
                name: 'author',
                fieldLabel: 'Author',
                xtype: 'textfield',
                allowBlank: false
            }, {
                name: 'borrowedBy',
                fieldLabel: 'Borrowed by',
                xtype: 'textfield',
                readOnly: true
            }
        ]
    }],

    initComponent: function() {
        var me = this;
        me.callParent();
        me.addEvents('change');
    }
});

