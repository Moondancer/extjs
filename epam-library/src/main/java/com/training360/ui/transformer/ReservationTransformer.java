package com.training360.ui.transformer;

import com.training360.backend.domain.Reservation;
import com.training360.ui.entity.ReservationItem;
import com.training360.ui.entity.ReservationRequest;

import javax.xml.bind.DatatypeConverter;
import java.util.Calendar;
import java.util.Date;

public class ReservationTransformer {    
    
    public ReservationItem transform(Reservation reservation) {
        ReservationItem reservationItem = new ReservationItem();
        reservationItem.setUser(reservation.getUser());
        reservationItem.setDeadline(toString(reservation.getDeadline()));
        reservationItem.setComment(reservation.getComment());
        return reservationItem;
    }
    
    public Reservation transform(ReservationRequest reservationRequest) {
        Reservation reservation = new Reservation();
        reservation.setDeadline(toDate(reservationRequest.getDeadline()));
        reservation.setComment(reservationRequest.getComment());
        return reservation;
    }
    
    private Date toDate(String s) {
        return DatatypeConverter.parseDateTime(s).getTime();        
    }
    
    private String toString(Date d) {
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        return DatatypeConverter.printDateTime(c);
        
    }

}
