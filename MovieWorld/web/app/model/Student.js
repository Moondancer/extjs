/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('MovieWorld.model.Student', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'id', type: 'int'
    }, {
        name: 'name', type: 'string'
    }, {
        name: 'height', type: 'float'
    }, {
        name: 'dob', type: 'date'
    }, {
        name: 'email', type: 'string'
    }],

    validations: [
        { type: 'email', field: 'email'}
    ],
    
    validate: function() {
        var err = this.callParent(arguments);
        if (this.data.height > 3) {
            err.add({
                field  : 'height',
                message: 'height must be less than 3'
            });
        }
        return err;
    },
    
    hasMany: {
        model: 'MovieWorld.model.Grade',
        name: 'grades'
    }
});

Ext.define('MovieWorld.model.Grade', {
    extend: 'Ext.data.Model',
    fields: ['assesment', 'comment', 'studentId'],
    
    belongsTo: {
        model: 'MovieWorld.model.Student',
        name: 'student',
        getterName: 'getStudent',
        foreignKey: 'studentId'
    }
});
