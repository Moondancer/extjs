package com.training360.backend.service;

import javax.transaction.Transactional;

import com.training360.backend.dao.ReservationRepository;
import com.training360.backend.domain.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.training360.backend.dao.BookRepository;
import com.training360.backend.domain.Book;

import java.util.HashMap;
import java.util.Map;

@Service
public class BookService {

	private BookRepository bookRepository;
    
    private ReservationRepository reservationRepository;
	
	@Autowired	
	public BookService(BookRepository bookRepository, ReservationRepository reservationRepository) {
		super();
		this.bookRepository = bookRepository;
        this.reservationRepository = reservationRepository;
    }
	
	public Page<Book> listBooks(int start, int limit, String query) {
            return bookRepository.findBooksByQuery(query, new PageRequest(start, limit));
	}
	
	public Book findBookById(long bookId) {
		Book book = bookRepository.findOne(bookId);
		if (book == null) {
			throw new BookException(String.format("Book not found with id: %s", bookId), HttpStatus.NOT_FOUND);
		}
		else {
			return book;
		}
	}
	
	@Transactional
	public Book createBook(Book book) {
//           book2 = new Book();
//           book2.setT
        validate(book);
        Book existingBook = bookRepository.findBookByAuthorAndTitle(book.getAuthor(), book.getTitle());
        if (existingBook != null) {
            throw new BookException("Book already exists with this author and title", HttpStatus.BAD_REQUEST);
        }
		return bookRepository.save(book);
	}

    @Transactional
    public Book editBook(long bookId, Book book) {
        validate(book);
        Book savedBook = bookRepository.findOne(bookId);
        if (book == null) {
            throw new BookException(String.format("Book not found with id: %s", bookId), HttpStatus.NOT_FOUND);
        }
        Book existingBook = bookRepository.findBookByAuthorAndTitle(book.getAuthor(), book.getTitle());
        if ((existingBook != null) && (existingBook != savedBook)) {
            throw new BookException("Book already exists with this author and title", HttpStatus.BAD_REQUEST);
        }
        savedBook.setTitle(book.getTitle());
        savedBook.setAuthor(book.getAuthor());
        return savedBook;
    }

    @Transactional
    public void deleteBook(long bookId) {
        Book bookToDelete = bookRepository.findOne(bookId);
        if (bookToDelete == null) {
            throw new BookException(String.format("Book not found with id: %s", bookId), HttpStatus.NOT_FOUND);
        }
        bookRepository.delete(bookToDelete);
    }

    public Page<Book> listReservedBooks(int start, int limit, String query) {
        return bookRepository.findBooksWithReservations(query, new PageRequest(start, limit));
    }

    @Transactional
    public Book reserve(long bookId, String user, Reservation reservation) {
        Book book = bookRepository.findBookWithReservations(bookId);
        if (book == null) {
            throw new BookException(String.format("Book not found with id: %s", bookId), HttpStatus.NOT_FOUND);
        }
        Reservation reservationToSave = new Reservation();
        reservationToSave.setUser(user);
        reservationToSave.setDeadline(reservation.getDeadline());
        reservationToSave.setComment(reservation.getComment());
        book.addReservation(reservationToSave);
        reservationRepository.save(reservationToSave);
        return book;
    }
    
      @Transactional
    public Book editReservation(long bookId, String user, Reservation reservation) {
        Book book = bookRepository.findBookWithReservations(bookId);
        if (book == null) {
            throw new BookException(String.format("Book not found with id: %s", bookId), HttpStatus.NOT_FOUND);
        }
        Reservation reservationToEdit = book.findReservationByUser(user);
        reservationToEdit.setDeadline(reservation.getDeadline());
        reservationToEdit.setComment(reservation.getComment());
        return book;
    }

    @Transactional
    public Book cancelReservation(long bookId, String user) {
        Book book = bookRepository.findBookWithReservations(bookId);
        if (book == null) {
            throw new BookException(String.format("Book not found with id: %s", bookId), HttpStatus.NOT_FOUND);
        }
        Reservation reservation = book.removeReservationByUser(user);
        reservationRepository.delete(reservation);
        return book;
    }

    @Transactional
    public Book borrow(long bookId, String user) {
        Book book = bookRepository.findOne(bookId);
        if (book == null) {
            throw new BookException(String.format("Book not found with id: %s", bookId), HttpStatus.NOT_FOUND);
        }
        if (book.getBorrowedBy() != null) {
            throw new BookException("The book already has been borrowed.", HttpStatus.BAD_REQUEST);
        }
        book.setBorrowedBy(user);
        return book;
    }

    @Transactional
    public Book returnBook(long bookId) {
        Book book = bookRepository.findOne(bookId);
        if (book == null) {
            throw new BookException(String.format("Book not found with id: %s", bookId), HttpStatus.NOT_FOUND);
        }
        if (book.getBorrowedBy() == null) {
            throw new BookException("The book has not been borrowed yet.", HttpStatus.BAD_REQUEST);
        }
        book.setBorrowedBy(null);
        return book;
    }

    public Page<Book> listMyBooks(String user, int start, int limit, String query) {
        return bookRepository.findMyBooks(user, query, new PageRequest(start, limit));
    }
    
    private void validate(Book book) {
        Map<String, String> errors = new HashMap<>();
        if (book.getAuthor() == null || isEmpty(book.getAuthor())) {
            errors.put("author", "Author is required.");
        }
        if (book.getTitle() == null || isEmpty(book.getTitle())) {
            errors.put("title", "Title is required.");
        }
        if (!errors.isEmpty()) {
            throw new BookException("Invalid values", HttpStatus.BAD_REQUEST, errors);
        }
    }
    
    private boolean isEmpty(String s) {
        return s == null || s.trim().isEmpty();        
    }
}
