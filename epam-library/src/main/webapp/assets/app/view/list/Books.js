/**
 * Created by Mark Kalinovits on 2015.02.24..
 */

Ext.define(
    'Library.view.list.Books',
    {
        alias: 'widget.booklist',
        extend: 'Ext.view.View',
        store: 'Books',
        itemSelector: 'div.wrap',
        autoScroll: true,

        tpl: new Ext.XTemplate(
            '<div>' +
            '<tpl for=".">' +
            '<div class="wrap">' +
            '<p><b>Title: </b>{title}</p>' +
            '<p><span><em>Author: </em>{author}</span></p>' +
            '</div>' +
            '</tpl>' +
            '</div>'
        ),

        initComponent: function() {
            console.info('Initializing EPAM Library Project Book list...');
            var me = this;
            me.callParent();
            //me.getLayout().setActiveItem(1);
        }
    }
);

