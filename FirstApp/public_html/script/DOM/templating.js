/**
 * Created by Training on 2015.02.10..
 */

Ext.onReady(
    function() {
        var data = [
            {
                name: "Liem Nieson",
                nationality: "Irish",
                awards: ["Irish Film", "British Independent"],
                films: [
                    {title: 'Rob Roy', year: 1991},
                    {title: 'Star Wars, The Phantom Menace', year: 1999}
                ]
            },
            {
                name: "Evan McGregor",
                nationality: "Scotish",
                awards: ["Best Actor in a Scottish Film", "Best British Actor", "Actor of the Year"],
                films: [
                    {title: 'Trainspotting', year: 1996},
                    {title: 'Star Wars, The Phantom Menace', year: 1999},
                    {title: 'Star Wars, The Attack of the Clones', year: 2002}
                ]
            }
        ];

        var myTemplate = new Ext.XTemplate(
            '<tpl for="." between="---">',
                '<h3>',
                    'Name: {name}',
                    '<tpl if="this.hasSeveralAwards(awards)">',
                        ' /<small>VERY GOOD!</small>/',
                    '</tpl>',
                '</h3>',
                '<p>Nationality: {nationality}</p>',
                '<p>Awards: {awards}</p>',
                '<p>Awards:<br/>',
                    '<tpl for="awards">',
                    '<div> - {.}</div>',
                    '</tpl>',
                '</p>',
                '<p>Films:<br/>',
                    '<tpl for="films">',
                    '<div> - {#}. {title}</div>',
                    '</tpl>',
                '</p>',
            '</tpl>',
            {
                hasSeveralAwards: function(awards) {
                    return awards.length > 2;
                }
            }
        );
        myTemplate.overwrite('main', data);
    }
);
