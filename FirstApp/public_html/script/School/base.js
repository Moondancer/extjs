/*
 * Variables
 */
var app;
var user;

/*
 * Functions
 */
function startSplash(target){
    // Starting splash screen
    var login = SchoolApp.view.Login.create();
    target.splashScreen = Ext.getBody().mask(login.show());
    target.splashScreen.fadeIn(0);
}

function removingSplash(target) {
    // Removing splash screen
    target.splashScreen.fadeOut({
        duration: 2000,
        remove: true
    });
    target.splashScreen.next().fadeOut({
        duration: 2000,
        remove: true
    });
}



/*
 * Application
 */
Ext.application(
    {
        name: 'SchoolApp',
        extend: 'Ext.app.Application',
        splashScreen: {},
        launch: function () {
            SchoolApp.view.Viewport.create();
        },
        init: function() {
            app = this;
            startSplash(this);
            initLogin();
        }
    }
);
