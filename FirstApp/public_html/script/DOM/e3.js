/**
 * Created by Training on 2015.02.10..
 */

Ext.onReady(
    function() {
        // DOM
        var loginInfo = Ext.get('logininfo');
        var menu = Ext.get('menu');
        var main = Ext.get('main');

        // Templates
        var newsTemplate = new Ext.XTemplate(
            '<tpl for="." between="---">' +
                '<h2>{title}</h2>' +
                '<p>' +
                    '{content} ' +
                    '<a href="{link}">See more...</a>' +
                '</p>' +
            '</tpl>'
        );

        // Data
        var news = [
            {
                title: "Take a second shot exam",
                content: "You can take your next exam in our PearsonVUE exam center without risk. If you fail you have a second chance free.",
                link: "secondshot.html"
            },
            {
                title: "LSI, I mean Avago ups Syncro speed to 12Gbps",
                content: "The part of Avago that was formerly LSI has announced a big update to their Syncro line with the new 9300 series.",
                link: "lsiavago.html"
            }
        ];

        Ext.Msg.prompt('Your username', 'Please enter your username:', function(btn, text){
            if (btn == 'ok'){
                Ext.Msg.show({
                    title:'Welcome',
                    msg: 'Welcome ' + text + '!',
                    buttons: Ext.Msg.OKCANCEL,

                    fn: function(btn) {
                        if(btn =="cancel") {
                            Ext.DomHelper.append(
                                "logininfo",
                                {
                                    tag: "a",
                                    id: "message",
                                    href: "login.html",
                                    html: "Login"
                                }
                            );
                        } else {
                            Ext.DomHelper.append(
                                "logininfo",
                                {
                                    tag: "h2",
                                    id: "message",
                                    html: "Welcome " + text + "!"
                                }
                            );
                            Ext.DomHelper.append(
                                "menu",
                                {
                                    tag: "nav",
                                    id: "nav-menu",
                                    children: [
                                        {tag: 'a', cls: 'menu-item', html: "Contact", href: "#"},
                                        {tag: 'a', cls: 'menu-item', html: "About Us", href: "#" }
                                    ]
                                }
                            );
                            if(text == "Admin") {
                                Ext.DomHelper.append(
                                    "nav-menu",
                                    {
                                        tag: 'a',
                                        cls: 'menu-item',
                                        html: "Settings",
                                        href: "#"
                                    }
                                );
                                Ext.DomHelper.append(
                                    "nav-menu",
                                    {
                                        tag: 'a',
                                        cls: 'menu-item',
                                        html: "Users",
                                        href: "#"
                                    }
                                );
                            }
                        }

                        // Styling
                        menuItems = Ext.select(".menu-item");
                        menuItems.setStyle(
                            {
                                backgroundColor: '#D39D9D',
                                border: '2px solid #520707',
                                borderRadius: '4px',
                                display: 'inline',
                                margin: '10px',
                                padding: '5px'
                            }
                        );

                        // News
                        newsTemplate.overwrite(
                            "main",
                            news
                        );
                    }
                });
            }
        });
    }
);
