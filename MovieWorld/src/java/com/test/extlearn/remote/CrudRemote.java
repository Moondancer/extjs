/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.remote;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.extlearn.remote.model.Filter;
import com.test.extlearn.remote.model.Sort;
import com.test.extlearn.service.CrudService;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author HizsnyaiG
 */
public abstract class CrudRemote<E, I extends Serializable> {
    
    public abstract CrudService<E, I> getService();
    
/*    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> listAll() {
        List<E> list = this.getService().findAll();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", list);
        map.put("total", list.size());
        map.put("success", true);
        return map;
    }*/
    
    @RequestMapping(value="{id}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> findOne(@PathVariable(value = "id") I id) {
        E e = this.getService().findOne(id);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", e);
        map.put("total", e != null ? 1 : 0);
        map.put("success", true);
        return map;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> create(@RequestBody E e) {
        E ce = this.getService().create(e);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", ce);
        map.put("total", ce != null ? 1 : 0);
        map.put("success", true);
        return map;
    }
    
    @RequestMapping(value="{id}", method = RequestMethod.PUT)
    @ResponseBody
    public Map<String, Object> update(@PathVariable(value = "id") I id, @RequestBody E e) {
        E ce = this.getService().update(e);
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", ce);
        map.put("total", ce != null ? 1 : 0);
        map.put("success", true);
        return map;
    }

    @RequestMapping(value="{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public void delete(@PathVariable(value = "id") I id) {
        this.getService().delete(id);
    }
    
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> filter(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "limit", required = false) Integer limit,
            @RequestParam(value = "sort", required = false) String s,
            @RequestParam(value = "filter", required = false) String f) throws Exception {
        String sortBy = null;
        String dir = null;
        String filterBy = null;
        String value = null;
        
        Sort[] sort = s != null ? new ObjectMapper().readValue(s, Sort[].class) : null;
        if (sort != null && sort.length > 0) {
            sortBy = sort[0].getProperty();
            dir = sort[0].getDirection();
        }
        
        Filter[] filter = f != null ? new ObjectMapper().readValue(f, Filter[].class) : null;
        if (filter != null && filter.length > 0) {
            filterBy = filter[0].getProperty();
            value = filter[0].getValue();
        }
        
        Long n = this.getService().count(filterBy, value);
        if (page != null)
            page--;
        List<E> list = this.getService().filter(filterBy, value, page, limit, sortBy, dir);
        
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("data", list);
        map.put("total", n);
        map.put("success", true);
        return map;
    }
}
