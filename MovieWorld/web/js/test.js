Ext.define('Actor', {
    extend: 'Ext.data.Model',
    idProperty: 'actorId',
    fields: ['imdb', 'name', 'gender',
        {
            name: 'birthday',
            type: 'date'
        }, 'alive'],
    
    hasMany: {
        model: 'Role',
        filterProperty: 'actorId'
    },
    
    proxy: {
        type: 'rest',
        url: 'service/actors',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('Movie', {
    extend: 'Ext.data.Model',
    idProperty: 'movieId',
    fields: ['imdb', 'title', 'year'],
    
    hasMany: {
        model: 'Role',
        filterProperty: 'movieId'
    },
    
    proxy: {
        type: 'rest',
        url : 'service/movies',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('Role', {
    extend: 'Ext.data.Model',
    idProperty: 'roleId',
    fields: ['actorId', 'movieId', 'characterName'],
    
    associations: [
        { type: 'belongsTo', model: 'Movie', foreignKey: 'movieId' },
        { type: 'belongsTo', model: 'Actor', foreignKey: 'actorId' }
    ],
    
    proxy: {
        type: 'rest',
        url: 'service/roles',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});

Ext.define('ActorStore', {
    extend: 'Ext.data.Store',
    model: 'Actor',
    autoSync: true,
    autoLoad: true,
    remoteSort: true
});

Ext.define('MovieStore', {
    extend: 'Ext.data.Store',
    model: 'Movie',
    autoSync: true,
    autoLoad: true,
    remoteSort: true
});

Ext.onReady(function() {
    Ext.create('Ext.grid.Panel', {
        store: Ext.create('ActorStore'),
        width: 400,
        height: 300,
        
        columns: [{
                text: 'imdb',
                dataIndex: 'imdb'
            }, {
                text: 'name',
                dataIndex: 'name'
            }, {
                text: 'gender',
                dataIndex: 'gender'
            }, {
                text: 'birthday',
                dataIndex: 'birthday'
            }],
        
        renderTo: Ext.getBody()
    });
});