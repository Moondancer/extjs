
Ext.define(
    'SchoolApp.view.Viewport',
    {
        extend: 'Ext.container.Viewport',
        layout: 'card',
        initComponent: function () {
            this.items = [{
                xtype: "panel",
                layout: 'card',
                items: [
                    {
                        xtype: 'panel',
                        layout: 'border',
                        items: [
                            {
                                xtype: "panel",
                                region: "west",
                                html: "<ul><li>New Student</li><li>Enroll Student</li></ul>",
                                flex: 1},
                            {
                                xtype: "panel",
                                layout: "border",
                                region: "center",
                                flex: 3,
                                items: [
                                    {
                                        xtype: "panel",
                                        layout: "border",
                                        region: "north",
                                        html: "Center North",
                                        flex: 1,
                                        items: [
                                            {xtype: "panel", region: "center", html: "<h1>School of Future</h1><h2>Class 4B</h2>", flex: 2},
                                            {xtype: "panel", region: "east", html: "<ul><li>Welcome, XY</li><li>Log off</li><li>Change password</li></ul>", flex: 1}
                                        ]
                                    },
                                    {
                                        xtype: "panel",
                                        region: "center",
                                        html: "<ul><li><img src='placeholder1.gif' /> Name of Student 1</li><li><img src='placeholder1.gif' /> Name of Student 2</li><li><img src='placeholder1.gif' />  Name of Student 3</li></ul>",
                                        flex: 4

                                    }
                                ]
                            },
                        ]
                    } ,
                    {
                        xtype: 'panel',
                        html: "OMG"
                    }
                ]

            }];
            this.callParent();
        }
    });
