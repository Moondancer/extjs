/**
 * Created by Training on 2015.02.11..
 */

function getTime1() {
    Ext.fly('time1').update(Ext.Date.format(new Date(), 'g:i:s A'));
}
function getTime2() {
    Ext.fly('time2').update(Ext.Date.format(new Date(), 'g:i:s A'));
}
function getTime5() {
    Ext.fly('time5').update(Ext.Date.format(new Date(), 'g:i:s A'));
}
function getTime(place) {
    Ext.fly(place).update(Ext.Date.format(new Date(), 'g:i:s A'));
}

Ext.onReady(
    function() {
        var runner = new Ext.util.TaskRunner();
        runner.start(
            {
                run: getTime1,
                interval: 1000
            }
        );
        runner.start(
            {
                run: getTime2,
                interval: 2000
            }
        );
        runner.start(
            {
                run: getTime5,
                interval: 5000
            }
        );
        runner.start(
            {
                args: ["time10"],
                run: function(a) {getTime(a)},
                interval: 10000
            }
        );
    }
);