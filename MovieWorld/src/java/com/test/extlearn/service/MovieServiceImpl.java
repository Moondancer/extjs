/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import com.test.extlearn.domain.model.Movie;
import com.test.extlearn.domain.repository.MovieRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author HizsnyaiG
 */
@Service
public class MovieServiceImpl extends CrudServiceImpl<Movie, Integer> implements MovieService {
    
    @Autowired
    private MovieRepository movieRepository;

    public void setMovieRepository(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }
    
    @Override
    protected CrudRepository<Movie, Integer> getRepository() {
        return this.movieRepository;
    }

    public List<Movie> filter(String filter, String value, Integer page, Integer limit, String sort, String dir) {
        Sort s = sort != null && dir != null ? new Sort(Sort.Direction.valueOf(dir), sort) : null;
        Pageable p = new PageRequest(page, limit, s);
        return this.movieRepository.findAll(p).getContent();
    }

    public Long count(String filter, String value) {
        return this.movieRepository.count();
    }
    
}
