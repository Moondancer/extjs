/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.Loader.setConfig({enabled:true});

Ext.application({
    name: 'MovieWorld',
    appFolder: 'js',
    
    views: ['ActorGrid', 'MovieGrid', 'LoginForm', 'MainMenu', 'RoleGrid', 'MovieList'],
    models: ['Actor', 'Movie', 'Role'],
    controllers: ['Actor', 'Movie', 'Login'],
    stores: ['ActorStore', 'MovieStore', 'RoleStore'],

//    launch: function () {
//        var win = Ext.create('Ext.window.Window', {
//            title: 'Login',
//
//            width: 200,
//            modal: true,
//
//            closable: false,
//            layout: 'fit',
//            items: {
//                xtype: 'loginform',
//            },
//        }).show();
//    },

    autoCreateViewport: true
});