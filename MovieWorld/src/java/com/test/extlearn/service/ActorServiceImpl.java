/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import com.test.extlearn.domain.model.Actor;
import com.test.extlearn.domain.repository.ActorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author HizsnyaiG
 */
@Service
public class ActorServiceImpl extends CrudServiceImpl<Actor, Integer> implements ActorService {
    
    @Autowired
    private ActorRepository actorRepository;

    public void setActorRepository(ActorRepository actorRepository) {
        this.actorRepository = actorRepository;
    }
    
    @Override
    protected CrudRepository<Actor, Integer> getRepository() {
        return this.actorRepository;
    }

    public List<Actor> filter(String filter, String value, Integer page, Integer limit, String sort, String dir) {
        Sort s = sort != null && dir != null ? new Sort(Sort.Direction.valueOf(dir), sort) : null;
        Pageable p = page != null ? new PageRequest(page, limit, s) : null;
        return this.actorRepository.findAll(p).getContent();
    }
    
    public Long count(String filter, String value) {
        return this.actorRepository.count();
    }
    
}
