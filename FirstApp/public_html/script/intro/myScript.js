/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Ext.onReady(
//    Ext.Msg.prompt('Your username', 'Please enter your username:', function(btn, text){
//        if (btn == 'ok'){
//            Ext.Msg.show({
//                title:'Welcome',
//                msg: 'Welcome ' + text + '!',
//                buttons: Ext.Msg.OKCANCEL,
//
//                fn: function(btn) {
//                    if(btn =="cancel") {
//                        Ext.Msg.show({
//                            title:'Warning',
//                            msg: 'Sorry you have to login!<br/>Self destroying started...',
//
//                            fn: function(btn) {
//                                if(btn =="cancel") {
//                                    console.log('Cancel pressed');
//                                }
//                            }
//                        });
//                    }
//                }
//            });
//        }
//    })
//);

Ext.onReady(function() {
    var pres = Ext.Class.getDefaultPreprocessors();
    var posts = Ext.ClassManager.defaultPostprocessors;
    console.info(pres);
    console.info(posts);

    var person1 = Ext.create("FirstApp.script.Person");
    person1.setName("Mark");
    person1.on(
        "yearchange",
        function(t, newV, oldV){
            console.log("old: " + oldV + "\n new: " + newV)
        });
    person1.setYear(1983);

    /*
     * Rendering
     */
    Ext.Msg.alert("Greetings", "Hello " + person1.getName() + " (" + person1.getAge() + ") !");
});
