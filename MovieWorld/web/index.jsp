<%-- 
    Document   : index
    Created on : 2015.02.16., 9:30:38
    Author     : Training
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Movie World Application</title>
        <link href="extjs/resources/css/ext-all.css" rel="stylesheet" type="text/css"/>
        <script src="extjs/ext-all.js" type="text/javascript"></script>
        <script src="js/app.js" type="text/javascript"></script>
        <!--<script src="js/movieApp.js" type="text/javascript"></script>-->
    </head>
    <body>
        <div id='grid'></div>
    </body>
</html>
