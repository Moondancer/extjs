/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('MovieWorld.view.RoleGrid', {
    extend: 'Ext.grid.Panel',
    store: Ext.create('MovieWorld.store.RoleStore'),
    
    alias: 'widget.rolegrid',

    tbar: [
        'Filter: ',
        {xtype: 'textfield', itemId: 'filterField'},
        {text: "Menu", menu: {xtype: 'mainmenu'}}

    ],
    columns: [{
            text: 'ID',
            dataIndex: 'roleId'
        }, {
            text: 'Actor ID',
            dataIndex: 'actorId'
        }, {
            text: 'Character',
            dataIndex: 'characterName',
            flex: 1
        }, {
            text: 'Movie ID',
            dataIndex: 'movieId'
        },
//        {
//            xtype: "combobox",
//            store: 'MovieStore',
//            editable: false,
//            displayField: "title",
//            valueField: "movieId",
//            itemId: "movieFilter"
//        }
    ]
});
