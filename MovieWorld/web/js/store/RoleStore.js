
Ext.define('MovieWorld.store.RoleStore', {
    extend: 'Ext.data.Store',
    model: 'MovieWorld.model.Role',
    autoSync: true,
    autoLoad: true,
//    remoteSort: true,
//    remoteFilter: true,
    data: []
});