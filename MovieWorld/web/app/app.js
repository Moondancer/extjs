/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.Loader.setConfig('enabled', true);

Ext.application({
    name: 'MovieWorld',
    appFolder: 'app',
    models: ['Student'],
    launch: function() {
        var student = MovieWorld.model.Student.create({
            id: '1',
            name: 'Béla',
            dob: '1973-01-12T00:00:00'
        });
        
        student.set('name', 'Józsi');
        console.log(student);
    }
});

