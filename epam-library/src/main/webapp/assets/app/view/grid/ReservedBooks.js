/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define(
    'Library.view.grid.ReservedBooks',
    {
        extend: 'Ext.grid.Panel',
        alias: 'widget.reservedbooksgrid',
        store: 'ReservedBooks',

        tbar: [
            {xtype: 'textfield', itemId: 'filterField', emptyText: 'Search book here...', flex:1}
        ],

        columns: [
            {
                text: 'title',
                dataIndex: 'title',
                flex: 4
            }, {
                text: 'author',
                dataIndex: 'author',
                flex: 5
            }
        ],
        dockedItems: [
            {
                xtype: 'pagingtoolbar',
                store: 'ReservedBooks',
                dock: 'bottom'
            }
        ],
        initComponent: function() {
            console.info('Initializing EPAM Library Project ReservedBooks grid view...');
            var me = this;
            me.callParent();
        }
    }
);