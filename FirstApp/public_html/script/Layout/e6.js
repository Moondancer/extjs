/**
 * Created by Training on 2015.02.12..
 */

var viewPort;
var menu;

Ext.define(
    'SchoolApp.view.Viewport',
    {
        extend: 'Ext.container.Viewport',
        layout: 'card',
        initComponent: function () {
            this.items = [{
                xtype: "panel",
                items: [
                    {
                        xtype: 'panel',
                        layout: 'border',
                        items: [
                            {
                                xtype: "panel",
                                region: "west",
                                html: "<ul><li>New Student</li><li>Enroll Student</li></ul>",
                                flex: 1},
                            {
                                xtype: "panel",
                                layout: "border",
                                region: "center",
                                flex: 3,
                                items: [
                                    {
                                        xtype: "panel",
                                        layout: "border",
                                        region: "north",
                                        html: "Center North",
                                        flex: 1,
                                        items: [
                                            {xtype: "panel", region: "center", html: "<h1>School of Future</h1><h2>Class 4B</h2>", flex: 2},
                                            {xtype: "panel", region: "east", html: "<ul><li>Welcome, XY</li><li>Log off</li><li>Change password</li></ul>", flex: 1}
                                        ]
                                    },
                                    {
                                        xtype: "panel",
                                        region: "center",
                                        html: "<ul><li><img src='placeholder1.gif' /> Name of Student 1</li><li><img src='placeholder1.gif' /> Name of Student 2</li><li><img src='placeholder1.gif' />  Name of Student 3</li></ul>",
                                        flex: 4

                                    }
                                ]
                            },
                        ]
                    } ,
                    {
                        xtype: 'panel',
                        html: "OMG"
                    }
                ]

            }];
            this.callParent();
    }
});

Ext.define(
    'SchoolApp.view.Login',
    {
        extend: "Ext.Window",
        title: "Login form",
        layout: "form",
        //height: 200,
        width: 200,
        defaultType: 'textfield',

        items: [{
            fieldLabel: 'Username',
            name: 'first',
            allowBlank:false
        },{
            fieldLabel: 'password',
            name: 'last',
            inputType: 'password'
        },{
            xtype: 'button',
            text: 'Login',
            name: 'login'
        }]
    }
);

function removingSplash(target) {
    // Removing splash screen
    target.splashScreen.fadeOut({
        duration: 10000,
        remove: true
    });
    target.splashScreen.next().fadeOut({
        duration: 10000,
        remove: true
    });
}

function startSplash(target){
    // Starting splash screen
    var login = SchoolApp.view.Login.create();
    target.splashScreen = Ext.getBody().mask(login.show());
    target.splashScreen.fadeIn(0);
}

Ext.application(
    {
        name: 'SchoolApp',
        extend: 'Ext.app.Application',
        splashScreen: {},
        launch: function () {
            //removingSplash(this);

            viewPort = SchoolApp.view.Viewport.create();
            Ext.defer(
                function(){
                    viewPort
                        .getLayout()
                        .next()
                },
                10000
            );

        },
        init: function() {
            //startSplash(this);
        }
    }
);