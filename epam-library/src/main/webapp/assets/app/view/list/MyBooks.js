/**
 * Created by Mark Kalinovits on 2015.02.26..
 */

Ext.define(
    'Library.view.list.MyBooks',
    {
        alias: 'widget.mybooklist',
        extend: 'Ext.view.View',
        store: 'MyBooks',
        itemSelector: 'div.wrap',
        autoScroll: true,

        tpl: new Ext.XTemplate(
            '<div>' +
            '<tpl for=".">' +
            '<div class="wrap">' +
            '<p><b>Title: </b>{title}</p>' +
            '<p><span><em>Author: </em>{author}</span></p>' +
            '</div>' +
            '</tpl>' +
            '</div>'
        ),

        initComponent: function() {
            console.info('Initializing EPAM Library Project MyBook list...');
            var me = this;
            me.callParent();
            //me.getLayout().setActiveItem(1);
        }
    }
);

