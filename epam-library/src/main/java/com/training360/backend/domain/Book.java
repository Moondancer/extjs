package com.training360.backend.domain;

import com.training360.backend.service.BookException;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.*;

@Entity
public class Book implements Serializable {

	private static final long serialVersionUID = 4359209467649975287L;

	@Id
	@GeneratedValue
	private Long id;
	
	private String title;

	private String author;

    @Column(name = "borrowed_by")
	private String borrowedBy;
    
    @OneToMany(mappedBy = "book")
    private List<Reservation> reservations;
    
    public void addReservation(Reservation reservation) {
        if (reservations == null) {
            reservations = new ArrayList<>();
        }
        reservations.add(reservation);
        reservation.setBook(this);
    }
    
    public Reservation findReservationByUser(String user) {
        for (Reservation reservation: reservations) {
            if (reservation.getUser().equals(user)) {
                return reservation;
            }
        }
        throw new BookException("There is no reservation for book. Book id: " + id + ", user: " + user, HttpStatus.NOT_FOUND);
    }
    
    public Reservation removeReservationByUser(String user) {
        Iterator<Reservation> i = reservations.iterator();
        while (i.hasNext()) {
            Reservation reservation = i.next(); 
            if (reservation.getUser().equals(user)) {
                i.remove();
                return reservation;
            }
        }
        throw new BookException("There is no reservation for book. Book id: " + id + ", user: " + user, HttpStatus.NOT_FOUND);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBorrowedBy() {
        return borrowedBy;
    }

    public void setBorrowedBy(String borrowedBy) {
        this.borrowedBy = borrowedBy;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
