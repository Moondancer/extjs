/**
 * Created by Training on 2015.02.09..
 */
Ext.define(
    'FirstApp.script.ooBasics.Student2',
    {
        extend: 'FirstApp.script.ooBasics.User',
        constructor: function(args) {
            this.initConfig(args);
            this.callParent();
            this.StudentID = this.self.getNextID();
        },
        config: {
            StudentID: null,
            TeacherID: null
        },
        statics: {
            autoID: 0,
            getNextID: function() {
                return ++this.autoID;
            }
        },
        EnrollInClass: function(){},
        RemoveFromClass: function(){},
        ValidatePassword: function(password){
            var result = false;
            if(typeof password == "string" &&
                password.length >= 6 ) {
                result = true;
            }

            return result;
        }
    }
);
