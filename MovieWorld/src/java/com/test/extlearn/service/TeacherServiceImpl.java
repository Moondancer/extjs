/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import com.test.extlearn.domain.model.Teacher;
import com.test.extlearn.domain.repository.TeacherRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author HizsnyaiG
 */
@Service("teacherService")
public class TeacherServiceImpl extends CrudServiceImpl<Teacher, Integer> implements TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    public void setTeacherRepository(TeacherRepository teacherRepository) {
        this.teacherRepository = teacherRepository;
    }
    
    @Override
    protected CrudRepository<Teacher, Integer> getRepository() {
        return this.teacherRepository;
    }

    public List<Teacher> filter(String filter, String value, Integer page, Integer limit, String sort, String dir) {
        return this.findAll();
    }

    public Long count(String filter, String value) {
        return this.teacherRepository.count();
    }
    
}
