/**
 * Created by Mark Kalinovits on 2015.02.27..
 */

Ext.define(
    'Library.Config',
    {

        singleton: true,
        alternateClassName: 'mcnf',

        config: {
            user: 'no-user',
            role: 'user'
        },
        roles: {
            admin: 'ROLE_ADMIN'
        },

        setData: function(data) {
            console.log(data);
        },
        isAdmin: function() {
            return this.getRole() === this.roles.admin;
        }
    }
);