/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.remote;

import com.test.extlearn.domain.model.Movie;
import com.test.extlearn.service.CrudService;
import com.test.extlearn.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author HizsnyaiG
 */
@Controller
@RequestMapping("movies")
public class MovieRemote extends CrudRemote<Movie, Integer> {
    @Autowired
    private MovieService movieService;

    public void setMovieService(MovieService movieService) {
        this.movieService = movieService;
    }
    
    @Override
    public CrudService<Movie, Integer> getService() {
        return this.movieService;
    }
    
}
