Ext.define('MovieWorld.view.grid.ActorGrid' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.ActorGrid',
    xtype : 'actorGrid',
    //title : 'Actor Data',
    //requires: ['MovieWorld.store.Actors'],
    store: 'Actors',
    
//    /**
//     * Custom function used for column renderer
//     * @param {Object} val
//     */
//    change: function(val) {
//        if (val > 0) {
//            return '<span style="color:green;">' + val + '</span>';
//        } else if (val < 0) {
//            return '<span style="color:red;">' + val + '</span>';
//        }
//        return val;
//    },
//    
//    /**
//     * Custom function used for column renderer
//     * @param {Object} val
//     */
//    pctChange: function(val) {
//        if (val > 0) {
//            return '<span style="color:green;">' + val + '%</span>';
//        } else if (val < 0) {
//            return '<span style="color:red;">' + val + '%</span>';
//        }
//        return val;
//    },
//
//    // render rating as "A", "B" or "C" depending upon numeric value.
//    rating: function(v) {
//        if (v == 0) return "A";
//        if (v == 1) return "B";
//        if (v == 2) return "C";
//    },
    
    viewConfig: {
        stripeRows: true
    },
    
    initComponent: function() {
       
        //this.store = 'Actors';
        console.log('Init ActorGrid');
        var me = this;
                
        me.columns = [{
            text     : 'Id',
            width    : 35,
            sortable : false,
            dataIndex: 'actorId'
        },
        {
            text     : 'Name',           
            flex     : 1,
            sortable : true,
            dataIndex: 'name'
        },
        {
            text     : 'Gender',
            width    : 55,
            sortable : true,
            dataIndex: 'gender'
        }
        ,
        {
            text     : 'Birthday',
            width    : 95,
            sortable : true,
            dataIndex: 'birthday',
            xtype: 'datecolumn', format:'Y-m-d'
        },
        {
            text     : 'Alive',
            width    : 95,
            sortable : true,
            dataIndex: 'alive',
            xtype: 'checkcolumn'
        }/*,
        {
            text     : '% Change',
            width    : 75,
            sortable : true,
            renderer : this.pctChange,
            dataIndex: 'pctChange'
        },
        {
            text     : 'Last Updated',
            width    : 85,
            sortable : true,
            renderer : Ext.util.Format.dateRenderer('m/d/Y'),
            dataIndex: 'lastChange'
        },
        {
            text: 'Rating',
            width: 30,
            sortable: true,
            renderer: this.rating,
            dataIndex: 'rating'
        }*/];

        //me.store = 'Actors';
        console.log(me.store);
        me.callParent();
        //this.callParent(arguments);
        
    }
});