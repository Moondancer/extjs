/**
 * Created by Training on 2015.02.09..
 */
Ext.define(
    'FirstApp.script.ooBasics.Student',
    {
        constructor: function(args) {
            this.initConfig(args);
        },
        config: {
            Assesment: null,
            AssesmentDate: new Date(),
            Comments: "",
            StudentID: null,
            SubjectName: ""
        }
    }
);
