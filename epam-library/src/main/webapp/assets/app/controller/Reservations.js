/**
 * Created by Mark Kalinovits on 2015.02.24..
 */

Ext.define('Library.controller.Reservations', {
    extend: 'Ext.app.Controller',

    views: ['grid.Reservations', 'grid.ReservedBooks'],
    models: ['Book', 'Reservation'],

    refs: [
        {ref: 'books',          selector: 'reservedbooksgrid'},
        {ref: 'reservations',   selector: 'reservationsgrid'}
    ],

    init: function () {
        this.task = new Ext.util.DelayedTask();
        this.control({
            'reservedbooksgrid': {
                itemclick: this.onItemClick
            },
            'reservationsgrid': {
                cancelReservation: this.onCancelReservation,
                editReservation: this.onEditReservation
            },
        });
    },

    onItemClick: function (grid, record) {
        console.info('Reservations controller onItemClick action...');
        var items = record.reservationItems();

        debugger;
        for(var i = 0; i < items.data.items.length; i++) {
            items.data.items[i].setBook(record);
        }

        this.getReservations().store.loadData(items.data.items);
    },

    onCancelReservation: function (record)
    {
        console.info('Reservations controller onCancelReservation action...');
        var me = this;

        var grid = me.getReservations();
        Ext.MessageBox.confirm('', 'Do you want to delet this reservation?', function (btn, text) {
            if (btn == 'yes') {
                var bookId = record.data.id;
                var user = mcnf.getUser();
                var url = 'http://localhost:8080/api/books/'+bookId+'/reservations/'+user;

                Ext.Ajax.request({
                    url: url,
                    method: 'DELETE',
                    success: function(response){
                        var text = response.responseText;
                        Ext.MessageBox.alert('Success', 'Reservation canceled.');
                        me.getBooks().getStore().reload();
                        me.getReservations().store.remove(record);
                    }
                });
            } else {
                console.log(grid.store);
                grid.store.rejectChanges();
            }
        }, me);

        //var books = me.getBooks();
        //console.log(books);
        //debugger;
        //books.getStore().sync();
    },
    onEditReservation: function(record) {
        console.info('Reservation controller onEditReservation action');
    }
});
