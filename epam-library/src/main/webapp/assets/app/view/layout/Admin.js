/**
 * Created by Mark Kalinovits on 2015.02.24.
 */

Ext.define(
    'Library.view.layout.Admin',
    {
        extend: 'Ext.tab.Panel',
        alias: 'widget.adminlayout',
        items: [
            {
                xtype: 'panel',
                title: 'Books',
                layout: 'border',
                items: [
                    {xtype: 'booksgrid', region:'center'},
                    {xtype: 'bookform', region: 'east'}
                ]
            },
            {
                xtype: 'panel',
                title: 'Reservations',
                layout: 'border',
                items: [
                    {xtype: 'reservedbooksgrid', region: 'center', flex: 3},
                    {xtype: 'reservationsgrid', region: 'south', flex: 2}
                ]
            }
        ],

        initComponent: function() {
            console.info('Initializing EPAM Library Project Admin layout...');
            var me = this;
            me.callParent();
            //me.getLayout().setActiveItem(1);
        }
    }
);