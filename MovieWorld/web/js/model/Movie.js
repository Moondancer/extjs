Ext.define('MovieWorld.model.Movie', {
    extend: 'Ext.data.Model',
    idProperty: 'movieId',
    fields: [
        'imdb',
        'title',
        {
            name: 'year',
            type: 'integer'
        }],
//    validations: [
//        {type: 'format', field: 'year', matcher: /^(1|2)(0-9){3}$/}   // WTF???
//    ],

    proxy: {
        type: 'rest',
        url: 'service/movies',
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
