Ext.define('MovieWorld.controller.Movie', {
    extend: 'Ext.app.Controller',
    views: ['MovieForm'],
    models: ['Movie'],
    refs: [{
            ref: 'grid', selector: 'moviegrid'
        }, {
            ref: 'form', selector: 'movieform'
        }
    ],
    init: function () {
        this.task = new Ext.util.DelayedTask();
        this.control({
            'moviegrid': {
                itemclick: this.onItemClick
            },
            'movieform #btnNew': {
                click: this.onNew
            },
            'movieform #btnRemove': {
                click: this.onRemove
            },
            'movieform #btnSave': {
                click: this.onSave
            },
            'movieform #btnCancel': {
                click: this.onCancel
            },
            'movieform': {
                change: this.onChange
            },
            'moviegrid #filterField': {
                change: this.onFilter
            },
        });
    },
    onItemClick: function (grid, record) {
        this.getForm().loadRecord(record);
        this.getForm().down('#btnRemove').enable();
    },
    onNew: function (btn) {
        this.getForm().loadRecord(this.getMovieModel().create());
        this.getForm().down('#btnRemove').disable();
    },
    onRemove: function (btn) {
        var grid = this.getGrid();
        var model = this.getForm().getRecord();
        Ext.MessageBox.confirm('Warning', 'Do you want to delet this record?', function (btn, text) {
            if (btn == 'yes') {
                grid.store.remove(model);
                this.getForm().getForm().reset();
            }
        }, this);

    },
    onSave: function (btn) {
        var form = this.getForm();

        var rec = form.getRecord();
        if (rec) {
            rec.set(form.getForm().getFieldValues());
            form.fireEvent('change', rec);
        }
    },
    onCancel: function (btn) {
        this.getForm().close();
    },
    onChange: function (r) {
        if (r.phantom) {
            this.getGrid().store.add(r);
        }
    },
    onFilter: function ()
    {
        this.task.delay(
                500,
                function () {
                    var store;
                    var field;

                    field = this.getGrid().down('#filterField');
                    store = this.getGrid().getStore();
                    store.clearFilter();
                    store.filter('title', field.value);
                },
                this
                );

    }
});
