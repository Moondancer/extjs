/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import java.io.Serializable;
import java.util.List;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author HizsnyaiG
 * @param <E>
 * @param <I>
 */
@Transactional(propagation = Propagation.SUPPORTS)
public interface CrudService<E, I extends Serializable> {

    @Transactional(propagation = Propagation.REQUIRED)
    public E create(E e);

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(E e);

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(I id);

    public List<E> findAll();

    public E findOne(I id);

    @Transactional(propagation = Propagation.REQUIRED)
    public E update(E e);
    
    public List<E> filter(String filter, String value, Integer page, Integer limit, String sort, String dir);
    
    public Long count(String filter, String value);
}
