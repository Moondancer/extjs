/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('MovieWorld.view.ActorForm', {
    extend: 'Ext.form.Panel',
    modal: true,
    title: 'Actor details',
    width: 300,
    height: 400,
    layout: 'fit',
    
    alias: 'widget.actorform',
    tbar: [{
            text: 'New',
            itemId: 'btnNew'
        }, '-', {
            text: 'Remove',
            itemId: 'btnRemove',
            disabled: true
        }, '->', {
            text: 'Save',
            itemId: 'btnSave',
            disabled: true,
            formBind: true,
        }
    ],
    items: [{
            xtype: 'form',
            bodyStyle: 'padding:5px',
            items: [
                {
                    name: 'actorId',
                    fieldLabel: "Id",
                    xtype: 'displayfield'
                }, {
                    name: 'imdb',
                    fieldLabel: 'IMDB',
                    xtype: 'textfield',
                }, {
                    name: 'gender',
                    fieldLabel: 'gender',
                    xtype: 'combobox',
                    store: ['male', 'female'],
                    allowBlank: false,
                    editable: false
                }, {
                    name: 'name',
                    fieldLabel: 'name',
                    xtype: 'textfield',
                    allowBlank: false
                }, {
                    name: 'alive',
                    fieldLabel: 'alive',
                    xtype: 'checkbox'
                }, {
                    name: 'birthday',
                    fieldLabel: 'birthday',
                    xtype: 'datefield'
                }, {
//                    name: 'roles',
                    itemId: "roleCombo",
                    fieldLabel: 'Roles',
                    xtype: 'combobox',
                    store: 'RoleStore',
                    displayField: 'characterName',
                    valueField: 'characterId',
                    queryMode: 'local'
                }]
        }],
    
    initComponent: function() {
        this.callParent();
        this.addEvents('change');
    },
    
//    buttons: [{
//            text: 'OK',
//            itemId: 'btnOk'
//        }, {
//            text: 'Cancel',
//            itemId: 'btnCancel'
//        }]
});
