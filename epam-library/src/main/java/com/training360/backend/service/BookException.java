package com.training360.backend.service;

import org.springframework.http.HttpStatus;

import java.util.Map;

public class BookException extends RuntimeException {

    private HttpStatus status;

    private Map<String, String> errors;

    public BookException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }
    
    public BookException(String message, HttpStatus status, Map<String, String> errors) {
        super(message);
        this.status = status;
        this.errors = errors;
    }
    
    public HttpStatus getStatus() {
        return status;
    }

    public Map<String, String> getErrors() {
        return errors;
    }
}
