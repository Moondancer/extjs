/**
 * Created by Mark Kalinovits on 2015.02.24..
 */

Ext.define(
    'Library.store.ReservedBooks',
    {
        extend: 'Ext.data.Store',
        model: 'Library.model.Book',
        autoSync: true,
        autoLoad: true,
        remoteSort: true,
        remoteFilter: true,
        proxy: {
            type: 'rest',
            url: 'http://localhost:8080/api/books/reservations',
            reader: {
                type: 'json',
                root: 'items',
                totalProperty: 'totalCount'
            }
        },
        initComponent: function() {
            console.info('Initializing EPAM Library Project Reservations store...');
            var me = this;
            me.callParent();
        }
    }
);
