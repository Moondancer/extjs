Ext.define('MovieWorld.store.MovieStore', {
    extend: 'Ext.data.Store',
    model: 'MovieWorld.model.Movie',
    autoSync: true,
    autoLoad: true,
    remoteSort: true
});
