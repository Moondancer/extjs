﻿Ext.define('MovieWorld.model.Role',{
    extend: 'Ext.data.Model',
    idPropery: 'roleId',
    fields: [
        {name: 'roleId'},
        {name: 'actorId'},
        {name: 'characterName'},
        {name: 'movieId'}
    ],
//    proxy: {
//        type: 'rest',
//        url: 'service/roles',
//        reader: {
//            type: 'json',
//            root: 'data'
//        }
//    }
});