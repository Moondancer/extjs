/**
 * Created by Mark Kalinovits on 2015.02.24..
 */

Ext.define(
    'Library.model.Reservation',
    {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'user', type: 'string'},
            {name: 'deadline', type: 'date'},
            {name: 'comment', type: 'string'}
        ],
        belongsTo: [{
            name: 'book',
            instanceName: 'book',
            model: 'Library.model.Book',
            getterName: 'getBook',
            setterName: 'setBook',
            associationKey: 'reservationItems',
            foreignKey: 'id'
        }],

        initComponent : function() {
            console.info('Initializing EPAM Library Project Reservation model...');
            var me = this;
            me.callParent();
        }
    }
);
