Ext.define('MovieWorld.controller.Actor', {
    extend: 'Ext.app.Controller',

    views: ['ActorForm'],
    models: ['Actor'],
    
    refs: [
        {ref: 'grid', selector: 'actorgrid'},
        {ref: 'form', selector: 'actorform'},
        {ref: 'roleCombo', selector: 'actorform #roleCombo'},
        {ref: 'roleGrid', selector: '#roleGrid'}
    ],
    
    init: function () {
        this.task = new Ext.util.DelayedTask();
        this.control({
            'actorgrid': {
                itemclick: this.onItemClick
            },
            'actorform #btnNew': {
                click: this.onNew
            },
            'actorform #btnRemove': {
                click: this.onRemove
            },
            
            'actorform #btnSave': {
                click: this.onSave
            },
            'actorform': {
                change: this.onChange
            },
            'actorgrid #filterField': {
                change: this.onActorFilter
            },
            'actorgrid #movieFilter': {
                change: this.onMovieFilter
            }
        });
    },

    onItemClick: function (grid, record) {
        this.getForm().loadRecord(record);
        this.getForm().down('#btnRemove').enable();
        this.getRoleGrid()
                .store
                .loadData(record.roleList()
                        .data
                        .items);
   },
    
    onNew: function (btn) {
        this.getForm().loadRecord(this.getActorModel().create());
        this.getForm().down('#btnRemove').disable();
    },
    
    onRemove: function(btn) {
        var grid = this.getGrid();
        var model = this.getForm().getRecord();
        Ext.MessageBox.confirm('Warning', 'Do you want to delet this record?', function (btn, text) {
            if (btn == 'yes') {
                grid.store.remove(model);
                this.getForm().getForm().reset();
            }
        }, this);
        
    },
    
    onSave: function (btn) {
        var form = this.getForm();

        var rec = form.getRecord();
        if (rec) {
            rec.set(form.getForm().getFieldValues());
            form.fireEvent('change', rec);
        }
    },
    
    onCancel: function(btn) {
        this.getForm().close();
    },

    onChange: function (r) {
        if (r.phantom) {
            this.getGrid().store.add(r);
        }
    },
    onActorFilter: function ()
    {
        this.task.delay(
                500,
                function () {
                    var store;
                    var field;

                    field = this.getGrid().down('#filterField');
                    store = this.getGrid().getStore();
                    store.clearFilter();
                    store.filter('name', field.value);
                },
                this
        );
        
    },
    onMovieFilter: function (cb, val) {
        console.log(val);
        this.getGrid().store.load({
            params: {
                movieId: val
            }
        });
    }
});