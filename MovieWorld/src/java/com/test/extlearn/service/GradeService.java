/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import com.test.extlearn.domain.model.Grade;

/**
 *
 * @author HizsnyaiG
 */
public interface GradeService extends CrudService<Grade, Integer> {
    
}
