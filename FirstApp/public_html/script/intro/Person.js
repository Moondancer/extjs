/**
 * Mark Kalivovits
 */

Ext.define(
    'FirstApp.script.Person',   // Class Name
    {   // Properties
        alias: "person",    // Use as alias
        xtype: "person",    // XType uses as alias
        mixins: {
            observable: 'Ext.util.Observable'
        },
        config: {
            name: "Dude",
            year: 2000
        },
        constructor: function(args) {
            this.initConfig(args);
            this.mixins.observable.constructor.call(this, args);    // Constructing Observable mixin!
        },
        getAge: function() {
            return new Date().getFullYear() - this.year;
        },
        applyYear: function(value) {
            if(typeof value !== 'number' || value < 1900 || value > new Date().getFullYear()) {
                console.error("Invalid birth year!")
                return;
            } 
            return value;
        },
        updateYear: function(newYear, oldYear) {
            this.fireEvent('yearchange', this, newYear, oldYear);   // Require: Observable.js, Need to initiate it in constructor!
        }
    }
);
