﻿Ext.define('MovieWorld.data.validations',{
    override : 'Ext.data.validations',
    singleton : true,
    
    imdbMessage : 'is not a valid IMDB actor id',
    imdbRe : /^\d{7}$/,
    imdb : function(config, value){
        return Ext.data.validations.imdbRe.test(value);
    },
	
    yearMessage : 'must be between 1870 and 9999',
    year : function(config, value){
        return value >= 1870 && value <= 9999;
    }
});