/**
 * Created by Mark Kalinovits on 2015.02.26.
 */

Ext.define(
    'Library.model.MyReservation',
    {
        extend: 'Ext.data.Model',
        fields: [
            {name: 'deadline', type: 'date'},
            {name: 'comment', type: 'string'}
        ],

        proxy: {
            type: 'rest',
            url: '',
            reader: {
                type: 'json',
                root: 'items',
                totalProperty: 'totalCount'
            },
            setUrl: function(id) {
                var me = this;
                var user = mcnf.getUser();
                var url       = 'http://localhost:8080/api/books/';

                url += id + '/reservations/' + user;
                console.log('MyReservation model: ' + url);
                me.url = url;
            }
        },
        initComponent : function() {
            console.info('Initializing EPAM Library Project MyReservation model...');
            var me = this;
            me.callParent();
        }
    }
);
