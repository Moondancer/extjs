/**
 * Created by Mark Kalinovits on 2015.02.23..
 */
Ext.define(
    'Library.view.window.Login',
    {
        extend: 'Ext.window.Window',
        alias: 'widget.loginwindow',

        title: 'Login',
        height: 200,
        width: 400,
        layout: 'fit',
        modal: true,
        items: [
            {
                xtype: 'form',
                defaultType: 'textfield',
                url: '/j_spring_security_check',
                items: [
                    {
                        xtype: 'text',
                        text: 'Please log in!'
                    },
                    {
                        name: 'username',
                        allowBlank: false,
                        emptyText: 'Username'
                    }, {
                        name: 'password',
                        inputType: 'password',
                        allowBlank: false,
                        emptyText: 'Password'
                    },
                    {
                        xtype: 'button',
                        text: 'Login',
                        itemId: 'btnLogin'
                    }
                ]
            }
        ],
        initComponent: function() {
            console.info('Initializing EPAM Library Project LoginWindow...');
            var me = this;
            me.callParent();
        }
    }
);