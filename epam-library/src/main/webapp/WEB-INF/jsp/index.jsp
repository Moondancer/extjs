<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>EPAM Library Project</title>
        <meta name="author" content="Mark Kalinovits">

        <link href="../../assets/extjs/resources/css/ext-all.css" rel="stylesheet" type="text/css"/>
        <link href="../../assets/resources/css/app.css" rel="stylesheet" type="text/css"/>
        <script src="../../assets/extjs/ext-all-dev.js" type="text/javascript"></script>
        <script src="../../assets/app/app.js" type="text/javascript"></script>
    </head>
    <body>
        <div id='main'>Some text we don't need here...</div>

        <script type="text/javascript">
            user_user = '<security:authentication property="principal.username" />';
            user_role = '<security:authentication property="principal.authorities" />';
        </script>
    </body>
</html>