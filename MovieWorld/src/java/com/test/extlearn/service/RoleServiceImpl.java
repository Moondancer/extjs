/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import com.test.extlearn.domain.model.Role;
import com.test.extlearn.domain.repository.RoleRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author HizsnyaiG
 */
@Service
public class RoleServiceImpl extends CrudServiceImpl<Role, Integer> implements RoleService {
    
    @Autowired
    private RoleRepository roleRepository;

    public void setRoleRepository(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }
    
    @Override
    protected CrudRepository<Role, Integer> getRepository() {
        return this.roleRepository;
    }

    public List<Role> filter(String filter, String value, Integer page, Integer limit, String sort, String dir) {
        Sort s = sort != null && dir != null ? new Sort(Sort.Direction.valueOf(dir), sort) : null;
        Pageable p = page != null ? new PageRequest(page, limit, s) : null;
        // TODO refactor to be more robust
        if (filter == null) {
            return this.roleRepository.findAll(p).getContent();
        } else if (filter.equalsIgnoreCase("actorId")) {
            return this.roleRepository.findByActorId(Integer.valueOf(value), p).getContent();
        }
        
        return this.roleRepository.findByMovieId(Integer.valueOf(value), p).getContent();
    }

    public Long count(String filter, String value) {
        if (filter == null) {
            return this.roleRepository.count();
        } else if (filter.equalsIgnoreCase("actorId")) {
            return this.roleRepository.countByActorId(Integer.valueOf(value));
        }
        
        return this.roleRepository.countByMovieId(Integer.valueOf(value));
    }
    
}
