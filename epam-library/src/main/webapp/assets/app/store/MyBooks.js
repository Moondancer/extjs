/**
 * Created by Mark Kalinovits on 2015.02.26..
 */

Ext.define(
    'Library.store.MyBooks',
    {
        extend: 'Ext.data.Store',
        model: 'Library.model.Book',
        autoSync: true,
        autoLoad: true,
        remoteSort: true,
        remoteFilter: true,
        proxy: {
            type: 'rest',
            url: 'http://localhost:8080/api/books/mybooks/',// Give it an URL to avoid the error

            // Replace the buildUrl method

            reader: {
                type: 'json',
                root: 'items',
                totalProperty: 'totalCount'
            },
            buildUrl: function(request) {

                var me = this;
                var operation = request.operation;
                var records   = operation.records || [];
                var record    = records[0];
                var url       = me.getUrl(request);
                var user      = mcnf.getUser();

                switch (request.action) {
                    case 'read':
                        url += user;
                        break;
                    default:
                        throw new Error("No such request action!");
                        break;
                }
                return url;
            }
        },
        initComponent: function() {
            console.info('Initializing EPAM Library Project Reservations store...');
            var me = this;
            me.callParent();
        }
    }
);
