/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define('Library.view.grid.Books', {
    extend: 'Ext.grid.Panel',
    store: 'Books',

    alias: 'widget.booksgrid',

    //title: 'Books',
    tbar: [
        {xtype: 'textfield', itemId: 'filterField', emptyText: 'Search book here...'}
    ],

    columns: [
        {
            text: 'id',
            dataIndex: 'id'
        }, {
            text: 'title',
            dataIndex: 'title',
            flex: 1
        }, {
            text: 'author',
            dataIndex: 'author'
        }, {
            text: 'Borrowed by',
            dataIndex: 'borrowedBy'
        }
    ],
    dockedItems: [
        {
            xtype: 'pagingtoolbar',
            store: 'Books',
            dock: 'bottom'
        }
    ],
    initComponent: function() {
        console.info('Initializing EPAM Library Project Books grid view...');
        var me = this;
        me.callParent();
    }
});