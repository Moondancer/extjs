Ext.define('MovieWorld.view.MovieGrid', {
    extend: 'Ext.grid.Panel',
    store: 'MovieStore',
    alias: 'widget.moviegrid',
    tbar: [
        'Filter: ',
        {xtype: 'textfield', itemId: 'filterField'}
    ],
    columns: [{
            text: 'IMDB',
            dataIndex: 'imdb'
        }, {
            text: 'Title',
            dataIndex: 'title'
        }, {
            text: 'Premier',
            dataIndex: 'year'
        }]
});
