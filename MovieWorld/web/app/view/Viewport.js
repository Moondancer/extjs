Ext.define('MovieWorld.view.Viewport',{
    extend: 'Ext.container.Viewport',
    layout: 'fit',
    requires: [
        'MovieWorld.view.grid.ActorGrid'
    ],
    initComponent : function(){
        var me = this;
        me.items = [{
            xtype: 'tabpanel',
            itemId: 'mainTabPanel',
            items: [{
                title: 'Actors',
                itemId: 'actorsTab',
                items: [{
                    xtype: 'actorGrid'
                }]
            }]
        }];
        me.callParent();
    }
});