/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import com.test.extlearn.domain.model.Student;
import com.test.extlearn.domain.repository.StudentRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author HizsnyaiG
 */
@Service("studentService")
public class StudentServiceImpl extends CrudServiceImpl<Student, Integer> implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public void setStudentRepository(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    protected CrudRepository<Student, Integer> getRepository() {
        return this.studentRepository;
    }

    public List<Student> filter(String filter, String value, Integer page, Integer limit, String sort, String dir) {
        return this.findAll();
    }

    public Long count(String filter, String value) {
        return this.studentRepository.count();
    }
    
}
