/**
 * Created by Mark Kalinovits on 2015.02.27..
 */

Ext.define(
    'Library.view.form.Reservation',
    {
        extend: 'Ext.form.Panel',
        alias: 'widget.reservationform',

        modal: true,
        bodyPadding: 15,

        defaults: {
            width: 160
        },
        items: [
            {
                xtype: 'text',
                text: 'Deadline',
                margin: '0 0 7 0'
            },
            {
                name:       'deadline',
                itemId:     'deadline',
                xtype:      'datefield',
                minValue:   new Date(),
                format:     'Y-m-d',
                allowBlank: false
            },
            {
                xtype: 'text',
                text: 'Comment',
                margin: '0 0 7 0'
            },
            {
                name:       'comment',
                itemId:     'comment',
                xtype:      'textareafield'
            },
            {
                xtype: 'button',
                text: 'Ok',
                itemId: 'btnOk',
                margin: '10 5 0 0',
                width: 75,
                handler: function() {
                    if(this.up('form').isValid()) {
                        var data = {
                            deadline: this.up('form').getValues().deadline,
                            comment: this.up('form').getValues().comment
                        };
                        this.up('form').fireEvent('submitForm', data, this.up('form').bookId, this.edit);
                    }
                }
            },
            {
                xtype: 'button',
                text: 'Cancel',
                itemId: 'btnCancel',
                margin: '10 0 0 5',
                width: 75,
                handler: function() {
                    this.up('window').close();
                }
            }
        ],

        initComponent: function() {
            var me = this;
            me.callParent();

            me.addEvents('submitForm');
        }
    }
);
