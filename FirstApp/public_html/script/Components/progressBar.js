/**
 * Created by Training on 2015.02.11..
 */

Ext.onReady(
    function() {
        var p = Ext.create(
            "Ext.ProgressBar",
            {
                width: 300,
                renderTo: Ext.getBody()
            }
        );

        p.updateProgress(.5, "still updating...", true);
        Ext.defer(function() {
            p.updateProgress(1, "Completed", true);
        }, 1000, this);

        //p.wait({
        //    interval: 50, //bar will move fast!
        //    duration: 500,
        //    increment: 10,
        //    text: 'Updating...',
        //    scope: this,
        //    fn: function(){
        //        p.updateText('Done!');
        //    }
        //});

        Ext.create(
            "Ext.panel.Panel",
            {
                title: "panel",
                headerPosition: "top",
                tools: [{type: "help"}],
                renderTo: Ext.getBody()
            }
        );
    }
);