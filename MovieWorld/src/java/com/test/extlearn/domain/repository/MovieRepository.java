/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.domain.repository;

import com.test.extlearn.domain.model.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author HizsnyaiG
 */
public interface MovieRepository extends CrudRepository<Movie, Integer> {
    
    public Page<Movie> findAll(Pageable p);
}
