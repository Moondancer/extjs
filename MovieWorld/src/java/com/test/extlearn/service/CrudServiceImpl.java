/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author HizsnyaiG
 * @param <E>
 * @param <I>
 */
public abstract class CrudServiceImpl<E, I extends Serializable> implements CrudService<E, I> {
    
    protected abstract CrudRepository<E, I> getRepository();
    
    @Override
    public E create(E e) {
        return getRepository().save(e);
    }
    
    @Override
    public E findOne(I id) {
        return getRepository().findOne(id);
    }
    
    @Override
    public List<E> findAll() {
        List<E> list = new ArrayList<E>();
        for (E e : getRepository().findAll())
            list.add(e);
        return list;
    }
    
    @Override
    public E update(E e) {
        return getRepository().save(e);
    }
    
    @Override
    public void delete(E e) {
        getRepository().delete(e);
    }
    
    @Override
    public void delete(I id) {
        getRepository().delete(id);
    }
}
