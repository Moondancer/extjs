package com.training360.ui.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wordnik.swagger.annotations.ApiModel;

import java.util.ArrayList;
import java.util.List;

@ApiModel
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BooksResponse {
    
    private Long totalCount;
    
    private List<BookResponseItem> items = new ArrayList<>();

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public List<BookResponseItem> getItems() {
        return items;
    }

    public void setItems(List<BookResponseItem> items) {
        this.items = items;
    }
    
    public boolean isSuccess() {
        return true;
        
    }
}
