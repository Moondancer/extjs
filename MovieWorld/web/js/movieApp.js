Ext.Loader.setConfig({enabled: true});

Ext.application({
    name: 'MovieWorld',
    appFolder: 'js',
    views: ['MovieGrid'],
    models: ['Movie'],
    controllers: ['Movie'],
    launch: function () {
        this.getMovieGridView().create({
            itemId: 'movieGrid',
            renderTo: 'grid',
            width: 400,
            height: 300
        });
    }
});
