/**
 * Created by Training on 2015.02.12..
 */

var win = Ext.create(
    "Ext.Window",
    {
        title: "Card Layout",
        layout: "card",
        height: 400,
        width: 400,
        items: [
            {
                xtype: "panel",
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                items: [
                    {xtype: "panel", title: "Inner Panel 1", html: "<h2>Card #1</h2><p>card two...</p>", flex: 1},
                    {xtype: "splitter"},
                    {xtype: "panel", title: "Inner Panel 2", html: "<h2>Card #2</h2><p>card two...</p>", flex: 2}
                ]
            },
            {
                xtype: "panel",
                layout: 'border',
                items: [
                    {xtype: "panel", region: "north", title: "North", html: "<h2>Card #3</h2><p>card two...</p>", flex: 1},
                    {xtype: "panel", region: "center", title: "Center", html: "<h2>Card #4</h2><p>card two...</p>", flex: 2},
                    {xtype: "panel", region: "south", title: "South", html: "<h2>Card #3</h2><p>card two...</p>", flex: 1},
                    {xtype: "panel", region: "east", title: "East", html: "<h2>Card #4</h2><p>card two...</p>", flex: 1},
                    {xtype: "panel", region: "west", title: "West", html: "<h2>Card #4</h2><p>card two...</p>", flex: 1}
                ]
            },
            {
                xtype: "panel",
                layout: {
                    type: 'accordion',
                    titleCollapse: true,
                    animate: true,
                    activeOnTop: false
                },
                items: [
                    {xtype: "panel", title: "Inner Panel 1", html: "<h2>Card #1</h2><p>card two...</p>"},
                    {xtype: "panel", title: "Inner Panel 2", html: "<h2>Card #2</h2><p>card two...</p>"}
                ]
            },
            {
                xtype: "panel",
                layout: 'column',
                items: [
                    {xtype: "panel", title: "Inner Panel 1", html: "<h2>Card #1</h2><p>card two...</p>", width: 200},
                    {xtype: "panel", title: "Inner Panel 2", html: "<h2>Card #2</h2><p>card two...</p>", columnWidth: 0.3},
                    {xtype: "panel", title: "Inner Panel 2", html: "<h2>Card #2</h2><p>card two...</p>", columnWidth: 0.7}
                ]
            },
            {
                xtype: "panel",
                layout: 'anchor',
                items: [
                    {xtype: 'panel', title: '75% Width and 20% Height', anchor: '75% 20%'},
                    {xtype: 'panel', title: 'Offset -300 Width & -200 Height', anchor: '-100 -200'},
                    {xtype: 'panel', title: 'Mixed Offset and Percent', anchor: '-200 20%'},
                    {xtype: 'panel', title: 'Mixed Offset and Percent', anchor: '-300 10%'}
                ]
            },
            {
                xtype: "panel",
                layout: 'absolute',
                defaultType: 'textfield',
                items: [
                    {x: 10, y: 10, xtype: 'label', text: '75% Width and 20% Height'},
                    {x: 80, y: 10, name: 'Offset -300 Width & -200 Height'},
                    {x: 10, y: 40, xtype: 'label', text: 'Mixed Offset and Percent'},
                    {x: 80, y: 40, name: 'Mixed Offset and Percent'}
                ]
            },
            {
                xtype: "panel",
                title: 'Table Layout',
                width: 300,
                height: 150,
                layout: {
                    type: 'table',
                    // The total column count must be specified here
                    columns: 3
                },
                defaults: {
                    // applied to each contained panel
                    bodyStyle: 'padding:20px'
                },
                items: [
                    {html: 'Cell A content', rowspan: 2},
                    {html: 'Cell B content', colspan: 2},
                    {html: 'Cell C content', cellCls: 'highlight'},
                    {html: 'Cell D content'}
                ]
            }
        ]
    }
);

Ext.onReady(
    function() {
        win.show();
        Ext.defer(function(){win.getLayout().next()}, 1000);
        Ext.defer(function(){win.getLayout().next()}, 2000);
        Ext.defer(function(){win.getLayout().next()}, 3000);
        Ext.defer(function(){win.getLayout().next()}, 4000);
        Ext.defer(function(){win.getLayout().next()}, 5000);
        Ext.defer(function(){win.getLayout().next()}, 6000);
        //Ext.defer(function(){win.getLayout().prev()}, 10000);
    }
);