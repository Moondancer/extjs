var loginWindow;

Ext.define(
    'SchoolApp.view.Login',
    {
        extend: "Ext.Window",
        title: "Login form",
        layout: "fit",
        //height: 200,
        width: 200,
        defaultType: 'textfield',
        items: [

        ]
    }
);

Ext.define(
    'SchoolApp.view.LoginForm',
    {
        extend: 'Ext.form.Panel',
        layout: "form",
        width: 200,
        defaultType: 'textfield',
        items: [{
            fieldLabel: 'Username',
            name: 'username',
            allowBlank:false
        },{
            fieldLabel: 'Password',
            name: 'password',
            inputType: 'password'
        }],
        buttons: [
            {
                text: 'Reset',
                handler: function() {
                    this.up('form').getForm().reset();
                }
            }, {
                text: 'Login',
                formBind: true, //only enabled once the form is valid
                disabled: true,
                handler: function() {
                    var form = this.up('form').getForm();
                    if (form.isValid()) {
                        loginProcess(form);
                    }
                    console.log(loginWindow);
                    loginWindow.hide();
                    //this.up('window').close(); // ???
                }
            }
        ],
        parentApp: {}
    }
);

/*
 * Functions
 */
function initLogin() {
     loginWindow = Ext.create('SchoolApp.view.Login');
    var loginForm = Ext.create('SchoolApp.view.LoginForm');
    loginWindow.add(loginForm);
    loginWindow.show();
}

function loginProcess(form) {
    var auth = login(form.getValues().username, form.getValues().username);
    if(auth) {
        removingSplash(app);
    } else {
        Ext.Msg.alert("Warning", "Unauthorized login!");
    }
}




