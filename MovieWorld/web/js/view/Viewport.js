Ext.define(
        'MovieWorld.view.Viewport',
        {
            extend: 'Ext.container.Viewport',
            layout: 'fit',
            requires: [
                'MovieWorld.view.ActorGrid',
                'MovieWorld.view.ActorForm',
                'MovieWorld.view.MovieGrid',
                'MovieWorld.view.MovieForm'
            ],
            items: [
                {
                    xtype: 'tabpanel',
                    activeTab: 0,
                    items: [
                        {
                            xtype: 'panel',
                            title: 'Actors',
                            layout: 'border',
                            items: [
                                {xtype: 'actorgrid', region: 'center'},
                                {
                                    region: 'east',
                                    width: 400,
                                    layout: 'border',
                                    items: [
                                        {xtype: 'actorform', region: 'center', padding: 15},
                                        {xtype: 'rolegrid', region: 'south', itemId: 'roleGrid'}
                                    ]
                                }
                            ]
                        }, {
                            xtype: 'panel',
                            title: 'Movies',
                            layout: 'border',
                            items: [
                                {xtype: 'movielist', region: 'center'},
                                {xtype: 'movieform', region: 'east', padding: 15}
                            ]
                        }, {
                            xtype: 'panel',
                            title: 'Roles',
                            layout: 'border',
                            items: [
                                {xtype: 'rolegrid', region: 'center'},
//                                {xtype: 'movieform', region: 'east', padding: 15}
                            ]
                        }
                    ]
                }
            ]
        }
);
