/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define(
    'Library.view.Start',
    {
        extend: 'Ext.Panel',
        alias: 'widget.start',

        requires: ['Library.Config'],

        layout: 'card',
        tbar: [
            {
                xtype: 'button',
                text: 'Start',
                menu: [
                    {
                        text: 'Administration',
                        itemId: 'adminMenu',
                    },
                    {
                        text: 'Borrowing',
                        itemId: 'userMenu'
                    }
                ]
            },
            '->',
            {
                itemId: "username",
                text: 'John Doe',
                padding: 10,
                menu: [
                    {
                        text: 'Logout',
                        handler: function() {
                            var redirect = '/j_spring_security_logout';
                            window.location = redirect;
                        }
                    }
                ]
            }
        ],
        items: [
            {xtype: 'userlayout'},
            {xtype: 'adminlayout'}
        ],

        initComponent: function() {
            console.info('Initializing EPAM Library Project Start...');
            var me = this;
            me.callParent();

            Ext.ComponentQuery.query('#username')[0].setText(mcnf.getUser());
            Ext.ComponentQuery.query('#adminMenu')[0].disabled = !mcnf.isAdmin();
        },
        loadAdmin: function() {
            var me = this;
            var layout = me.getLayout();
            layout.setActiveItem(1);
        },
        loadUser: function() {
            var me = this;
            var layout = me.getLayout();
            layout.setActiveItem(0);
        }
    }
);