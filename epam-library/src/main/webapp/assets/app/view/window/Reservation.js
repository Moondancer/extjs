/**
 * Created by Mark Kalinovits on 2015.02.27..
 */
Ext.define(
    'Library.view.window.Reservation',
    {
        extend: 'Ext.window.Window',
        alias: 'widget.reservationwindow',

        title: 'Reservation details',
        //height: 400,
        //width: 200,
        layout: 'fit',
        modal: true,
        minimizable: true,
        maximizable: true,

        items: [
            {xtype: 'reservationform'}
        ],

        initComponent: function() {
            console.info('Initializing EPAM Library Project Reservation window...');
            var me = this;
            me.callParent();

            this.down('form').bookId = this.bookId;
            this.down('form').edit = this.edit;
        },
        minimize: function() {
            console.warn('I will not minimize!');
        }
    }
);