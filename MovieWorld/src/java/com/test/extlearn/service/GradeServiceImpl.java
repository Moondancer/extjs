/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.extlearn.service;

import com.test.extlearn.domain.model.Grade;
import com.test.extlearn.domain.repository.GradeRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 *
 * @author HizsnyaiG
 */
@Service
public class GradeServiceImpl extends CrudServiceImpl<Grade, Integer> implements GradeService {
    @Autowired
    private GradeRepository gradeRepository;

    public void setGradeRepository(GradeRepository gradeRepository) {
        this.gradeRepository = gradeRepository;
    }
    
    @Override
    protected CrudRepository<Grade, Integer> getRepository() {
        return this.gradeRepository;
    }

    public List<Grade> filter(String filter, String value, Integer page, Integer limit, String sort, String dir) {
        return this.findAll();
    }

    public Long count(String filter, String value) {
        return this.gradeRepository.count();
    }
    
}
