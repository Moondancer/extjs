Ext.define('MovieWorld.view.LoginForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.loginform',
    items: [
        {
            xtype: 'form',
            bodyStyle: 'padding:5px',
            
            url: 'submit.jsp',
            items: [
                {
                    name: 'username',
                    fieldLabel: "Username",
                    xtype: 'textfield',
                    allowBlank: false
                }, {
                    name: 'password',
                    fieldLabel: 'Password',
                    xtype: 'textfield',
                    inputType: 'password',
                    allowBlank: false
                }
            ]
        }
    ],
    initComponent: function () {
        this.callParent();
//        this.addEvents('change');
    },
    buttons: [
        {
            text: 'Submit',
            itemId: 'btnSubmit'
        }
    ]
});

