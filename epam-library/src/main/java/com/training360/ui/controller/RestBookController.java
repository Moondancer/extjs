package com.training360.ui.controller;

import com.training360.backend.service.BookException;
import com.training360.backend.service.BookService;
import com.training360.ui.entity.*;
import com.training360.ui.transformer.BookTransformer;
import com.training360.ui.transformer.BooksResponseTransformer;
import com.training360.ui.transformer.ReservationTransformer;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/books")
@Api(value = "books", description = "Operations on books")
public class RestBookController {
	
    private BookService bookService;
	
	@Autowired
	public RestBookController(BookService bookService) {
		super();
		this.bookService = bookService;
	}

    @ApiOperation(value = "Retrieves the books")
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public BooksResponse books(@RequestParam(value = "page", required = false, defaultValue = "1") int start,
                               @RequestParam(value = "limit", required = false, defaultValue = "25") int limit,
                               @RequestParam(value = "query", required = false, defaultValue = "") String query
                               ) {
		return new BooksResponseTransformer().transform(bookService.listBooks(start-1, limit, query), false);
	}

    @ApiOperation(value = "Retrieves a book")
	@RequestMapping(value = "/{bookId}", method = RequestMethod.GET)
	public BooksResponse book(
            @ApiParam(value = "The id of the book", required = true) @PathVariable long bookId) {
		return new BooksResponseTransformer().transform(bookService.findBookById(bookId), false);
	}

    @ApiOperation(value = "Creates a new book")
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public BooksResponse createBook(
            @ApiParam(value = "Book to create", required = true) @RequestBody BookRequest bookRequest) {
        return new BooksResponseTransformer().transform(bookService.createBook(new BookTransformer().transform(bookRequest)), false);
    }

    @ApiOperation(value = "Edits a book")
    @RequestMapping(value = "/{bookId}", method = RequestMethod.PUT)
    public BooksResponse editBook(@ApiParam(value = "The id of the book", required = true) @PathVariable long bookId,
                         @ApiParam(value = "Edited book", required = true) @RequestBody BookRequest bookRequest) {
        return new BooksResponseTransformer().transform(bookService.editBook(bookId, new BookTransformer().transform(bookRequest)), false);
    }

    @ApiOperation(value = "Deletes a book")
    @RequestMapping(value = "/{bookId}", method = RequestMethod.DELETE)
    public void deleteBook(
            @ApiParam(value = "The id of the book", required = true) @PathVariable long bookId) {
        bookService.deleteBook(bookId);
    }

    @ApiOperation(value = "Gets the list of the reserved books")
    @RequestMapping(value = "/reservations", method = RequestMethod.GET)
    public BooksResponse reservations(@RequestParam(value = "page", required = false, defaultValue = "1") int start,
                                      @RequestParam(value = "limit", required = false, defaultValue = "25") int limit,
                                      @RequestParam(value = "query", required = false, defaultValue = "") String query) {
        return new BooksResponseTransformer().transform(bookService.listReservedBooks(start-1, limit, query), true);
    }

    @ApiOperation(value = "Creates a reservation")
    @RequestMapping(value = "{bookId}/reservations/{user}", method = RequestMethod.POST)
    public BooksResponse reserve(@PathVariable long bookId, @PathVariable String user, @RequestBody ReservationRequest reservationRequest) {
        return new BooksResponseTransformer().transform(bookService.reserve(bookId, user, new ReservationTransformer().transform(reservationRequest)), true);
    }
    
    @ApiOperation(value = "Edits a reservation")
    @RequestMapping(value = "{bookId}/reservations/{user}", method = RequestMethod.PUT)
    public BooksResponse editReservation(@PathVariable long bookId, @PathVariable String user, @RequestBody ReservationRequest reservationRequest) {
        return new BooksResponseTransformer().transform(bookService.editReservation(bookId, user, new ReservationTransformer().transform(reservationRequest)), true);
    }

    @ApiOperation(value = "Cancels a reservation")
    @RequestMapping(value = "{bookId}/reservations/{user}", method = RequestMethod.DELETE)
    public BooksResponse reserve(@PathVariable long bookId, @PathVariable String user) {
        return new BooksResponseTransformer().transform(bookService.cancelReservation(bookId, user), true);
    }

    @ApiOperation(value = "Borrows a book")
    @RequestMapping(value = "{bookId}/borrow/{user}", method = RequestMethod.POST)
    public BooksResponse borrow(@PathVariable long bookId, @PathVariable String user) {
        return new BooksResponseTransformer().transform(bookService.borrow(bookId, user), false);
    }

    @ApiOperation(value = "Returns a book")
    @RequestMapping(value = "{bookId}/borrow", method = RequestMethod.DELETE)
    public BooksResponse returnBook(@PathVariable long bookId) {
        return new BooksResponseTransformer().transform(bookService.returnBook(bookId), false);
    }

    @ApiOperation(value = "List the borrowed and reserved books for a user")
    @RequestMapping(value = "/mybooks/{user}", method = RequestMethod.GET)
    public BooksResponse myBooks(@PathVariable String user,
                                    @RequestParam(value = "page", required = false, defaultValue = "1") int start,
                                    @RequestParam(value = "limit", required = false, defaultValue = "25") int limit,
                                    @RequestParam(value = "query", required = false, defaultValue = "") String query) {
        return new BooksResponseTransformer().transform(bookService.listMyBooks(user, start-1, limit, query), true);
    }
    
    @ExceptionHandler(BookException.class)
    public ResponseEntity<ExceptionEntity> handleBookException(BookException ex) {
        HttpHeaders responseHeaders = new HttpHeaders();
        ExceptionEntity exceptionEntity = new ExceptionEntity(ex.getMessage(), ex.getErrors());
        return new ResponseEntity<>(exceptionEntity, responseHeaders, ex.getStatus());
    }
    
    
}
