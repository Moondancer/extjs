/**
 * Created by Mark Kalinovits on 2015.02.23..
 */

Ext.define('Library.controller.Login', {
    extend: 'Ext.app.Controller',
    refs: [
        {ref: 'login', selector: 'loginwindow'}
    ],
    init: function () {
        //console.info('Initializing EPAM Library Project Login controller...');
        //console.log(this.getLogin());

        this.control({
            'loginwindow #btnLogin': {
                click: this.onLogin
            }
        });
    },
    onLogin: function () {
        console.info('Initializing EPAM Library Project Login controller, onLogin action...');

        var login = this.getLogin();
        this.getLogin().down('form').submit({
            success: function (form, action) {
                Ext.Msg.alert('Success', action.result.msg);
                login.up('window').close();
            },
            failure: function (form, action) {
                switch (action.failureType) {
                    case Ext.form.action.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.action.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.action.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', action.result.msg);
                }
            }
        });
    }
});
